import express from 'express';
import cors from 'cors';
import { ApolloServer } from 'apollo-server-express';
import {
  ApolloServerPluginDrainHttpServer,
  ApolloServerPluginLandingPageGraphQLPlayground,
  ApolloServerPluginCacheControl,
  ApolloServerPluginLandingPageDisabled
} from 'apollo-server-core';
import responseCachePlugin from 'apollo-server-plugin-response-cache';
import { graphqlUploadExpress } from 'graphql-upload';
import { Server as SocketIO } from 'socket.io';
import { path as appRoot } from 'app-root-path';
import { createServer } from 'http';
import path from 'path';
import { commonConfig } from './common/config';
import { loggerMiddleware } from './common/logger';
import { typeDefs, resolvers } from './gql';
import { createMongoConnection } from './mongoose';
import { getTokenPayloadData, validateAdminToken } from './common/jwt';
import Redis from './common/redis';
import { initializeIO } from './socket';
import downloadRouter from './routers/download';

/** DB 커넥션 및 Http 서버 생성 */
const createHttpServer = async () => {
  const app = express();
  const httpServer = createServer(app);
  const io = new SocketIO(httpServer, { path: '/sock', cors: { origin: commonConfig.httpServer.cors } });
  
  /** Connect MongoDB */
  const { mongo, models } = await createMongoConnection();

  app.disable('x-powered-by');

  if (commonConfig.httpServer.cors) app.use(cors());
  app.use(express.json({ limit: '256mb' }));
  app.use(express.urlencoded({ extended: false }));
  app.use(commonConfig.graphql.path, graphqlUploadExpress({ maxFiles: 10 }));
  app.use(loggerMiddleware);
  app.use('/upload', express.static(path.join(appRoot, 'upload')));
  app.use('/download', downloadRouter(models));

  /** Connect Redis */
  const authCodeDb = new Redis(commonConfig.redis.database.authCode);
  const verificatedDb = new Redis(commonConfig.redis.database.verificated);
  await authCodeDb.connect();
  await verificatedDb.connect();
  const redis: RedisDB = {
    authCode: authCodeDb,
    verificated: verificatedDb
  };

  /** SocketIO Server 초기화 */
  initializeIO(io, models);

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    debug: commonConfig.graphql.debug,
    context: ({ req, res }): GraphQLContext => ({
      req,
      res,
      mongo,
      models,
      redis,
      io,
      auth: getTokenPayloadData(req.headers),
      isAdmin: validateAdminToken(req.headers)
    }),
    introspection: commonConfig.graphql.introspection,
    plugins: [
      ApolloServerPluginCacheControl({ defaultMaxAge: 0 }),
      responseCachePlugin(),
      ApolloServerPluginDrainHttpServer({ httpServer }),
      commonConfig.graphql.playground ?
        ApolloServerPluginLandingPageGraphQLPlayground() :
        ApolloServerPluginLandingPageDisabled()
    ]
  });

  await server.start();
  server.applyMiddleware({
    app,
    path: commonConfig.graphql.path,
    cors: commonConfig.httpServer.cors
  });

  return { httpServer, mongo };
};

export default createHttpServer;