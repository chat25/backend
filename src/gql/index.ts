import { commonTypeDefs, commonResolver } from './common';
import { userTypeDefs, userResolver } from './user';
import { friendsTypeDefs, friendsResolver } from './friends';
import { roomTypeDefs, roomResolver } from './room';
import { adminTypeDefs, adminResolver } from './admin';

export const typeDefs = [
  commonTypeDefs,
  userTypeDefs,
  friendsTypeDefs,
  roomTypeDefs,
  adminTypeDefs
];

export const resolvers = [
  commonResolver,
  userResolver,
  friendsResolver,
  roomResolver,
  adminResolver
];