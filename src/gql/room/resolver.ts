import _ from 'lodash';
import { enc, SHA256 } from 'crypto-js';
import moment from 'moment';
import archiver from 'archiver';
import { path as appRoot } from 'app-root-path';
import fs from 'fs';
import path from 'path';
import { finished, FinishedOptions } from 'stream';
import { generateUuidBuffer, gqlResolver } from '../../common/utils';
import { findUserSocket } from '../../socket';

const UPLOAD_PATH = path.join(appRoot, 'upload');

/** Stream 데이터 전송 완료 확인 */
const finishedAsync = (stream: fs.WriteStream | fs.ReadStream, options: FinishedOptions = {}) => (
  new Promise<boolean>((resolve, reject) => {
    finished(stream, options, err => {
      if (err) return reject(err);
      resolve(true);
    });
  })
);

/** [Mutation] 그룹/오픈 채팅방 생성 */
const createRoom = async (parent: any, args: CreateRoomArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<CreateRoomResult> => {
    const { auth, io, models: { User, Room, RoomJoinLeave, Chat } } = ctx;
    const { type, name, users, maxUser, isPrivate, password } = args;

    if (type === 'group') {
      const userUuidString = auth.uuid.toString('hex');
      const usersUuidBufferArr = users.filter(d => d !== userUuidString).map(d => Buffer.from(d, 'hex')).concat(auth.uuid);
  
      const isExistUsers = (await User.countDocuments({ uuid: { $in: usersUuidBufferArr } })) === usersUuidBufferArr.length;
  
      if (!isExistUsers) return {
        result: null,
        errCode: 'INVALID_USERS'
      };
    }

    const roomUuid = generateUuidBuffer();
    const joinDate = new Date();

    // 그룹채팅방 생성
    if (type === 'group') {
      const usersUuidArr = [auth.uuid, ...users.map(d => Buffer.from(d, 'hex'))];

      const roomUserInfoArr = await User.find({ uuid: { $in: usersUuidArr } }, { uuid: 1, nickname: 1 });
      let lastChatUuid: Buffer | null = null;

      await Chat.insertMany(roomUserInfoArr.map(d => ({
        uuid: lastChatUuid = generateUuidBuffer(),
        userUuid: d.uuid,
        roomUuid,
        type: 'join',
        sendDate: joinDate,
        read: usersUuidArr
      })));

      await Room.insertMany({
        uuid: roomUuid,
        type,
        users: usersUuidArr,
        blackList: [],
        lastChatUuid
      });

      await RoomJoinLeave.insertMany(usersUuidArr.map(d => ({
        userUuid: d,
        roomUuid,
        joinDate
      })));

      await User.updateMany({ uuid: { $in: usersUuidArr } }, { $push: { rooms: roomUuid } });

      const roomUuidString = roomUuid.toString('hex');

      usersUuidArr.forEach(async d => {
        const socket = findUserSocket(io, d);
        if (!socket) return;
        await socket.join(roomUuidString);
      });

      io.to(roomUuidString).emit('refresh-rooms');

      return {
        result: roomUuid.toString('hex'),
        errCode: null
      };
    }

    // 오픈채팅방 생성
    if (type === 'open') {
      const isExistRoom = !!(await Room.countDocuments({ name }));
      if (isExistRoom) return {
        result: null,
        errCode: 'DUPLICATED_ROOM_NAME'
      }

      const lastChatUuid = generateUuidBuffer();

      await Chat.insertMany({
        uuid: lastChatUuid,
        userUuid: auth.uuid,
        roomUuid,
        type: 'join',
        sendDate: joinDate,
        read: [auth.uuid]
      });

      await Room.insertMany({
        uuid: roomUuid,
        userUuid: auth.uuid,
        type,
        name,
        users: [auth.uuid],
        lastChatUuid,
        maxUser,
        isPrivate,
        password: password ? SHA256(password).toString(enc.Hex) : null,
        blackList: []
      });

      await RoomJoinLeave.insertMany({
        userUuid: auth.uuid,
        roomUuid,
        joinDate
      });

      await User.updateOne({ uuid: auth.uuid }, { $push: { rooms: roomUuid } });

      const roomUuidString = roomUuid.toString('hex');

      const socket = findUserSocket(io, auth.uuid);
      await socket.join(roomUuidString);
      socket.emit('refresh-rooms');

      return {
        result: roomUuidString,
        errCode: null
      };
    }

    return {
      result: null,
      errCode: 'SERVER_ERROR'
    };
  }, ctx)
);

/** [Query] 개인채팅방 정보 확인 */
const getPersonalRoomInfo = async (parent: any, args: GetPersonalRoomInfoArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetPersonalRoomInfoResult> => {
    const { auth, models: { User, Room, RoomJoinLeave, RoomName,Chat } } = ctx;
    const { userUuid } = args;
    const userUuidBuffer = Buffer.from(userUuid, 'hex');

    const isExistUser = !!(await User.countDocuments({ uuid: userUuidBuffer }));
    if (!isExistUser) return {
      result: null,
      errCode: 'INVALID_USER'
    };

    const roomInfo = await Room.findOne({
      type: 'personal',
      $and: [
        { users: { $elemMatch: { $eq: auth.uuid } } },
        { users: { $elemMatch: { $eq: userUuidBuffer } } }
      ]
    });
    if (!roomInfo || roomInfo.type !== 'personal') return {
      result: null,
      errCode: 'SERVER_ERROR'
    };

    const usersArr = await User.find({ uuid: { $in: roomInfo.users } });

    await Chat.updateMany(
      { roomUuid: roomInfo.uuid, read: { $not: { $elemMatch: { $eq: auth.uuid } } } },
      { $push: { read: auth.uuid } }
    );

    const roomJoinLeaveInfo = await RoomJoinLeave.findOne(
      { userUuid: auth.uuid, roomUuid: roomInfo.uuid },
      { joinDate: 1 }
    );
    if (!roomJoinLeaveInfo) return { result: null, errCode: 'SERVER_ERROR' };
    const { joinDate } = roomJoinLeaveInfo;

    const chatArr = await Chat.find({ roomUuid: roomInfo.uuid, sendDate: { $gte: joinDate } }).sort({ sendDate: -1 }).limit(100);

    const roomUserInfoArr = await User.find(
      { rooms: { $elemMatch: { $eq: roomInfo.uuid } } },
      { uuid: 1, nickname: 1, profileImage: 1, profileText: 1 }
    );

    const roomNameInfo = await RoomName.findOne({ userUuid: auth.uuid, roomUuid: roomInfo.uuid }, { roomName: 1 });
    const roomName = roomNameInfo?.roomName;

    const result: GetPersonalRoomInfoResult = {
      result: {
        uuid: roomInfo.uuid.toString('hex'),
        type: roomInfo.type,
        name: roomName || roomInfo.name || usersArr.filter(d => d.uuid.toString('hex') !== auth.uuid.toString('hex'))[0].nickname,
        users: usersArr.map(d => ({
          uuid: d.uuid.toString('hex'),
          nickname: d.nickname,
          profileImage: d.profileImage,
          profileText: d.profileText
        })),
        leaveUsers: null,
        createDate: roomInfo.createDate.toISOString(),
        messages: chatArr.map(d => ({
          uuid: d.uuid.toString('hex'),
          userUuid: d.userUuid.toString('hex'),
          roomUuid: d.roomUuid.toString('hex'),
          type: d.type,
          contents: ['join', 'leave'].indexOf(d.type) < 0 ? d.contents : _.find(usersArr, d => d.uuid.toString('hex') === auth.uuid.toString('hex')).nickname,
          filePath: d.filePath,
          sendDate: d.sendDate.toISOString(),
          read: d.read ? d.read.map(d => d.toString('hex')) : null
        })).reverse(),
        userNum: roomUserInfoArr.length
      },
      errCode: null
    };

    return result;
  }, ctx)
);

/** [Query] 채팅방 정보 조회 */
const getRoomInfo = async (parent: any, args: GetRoomInfoArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetRoomInfoResult> => {
    const { auth, models: { User, Room, RoomJoinLeave, RoomName, Chat } } = ctx;
    const { roomUuid } = args;
    const roomUuidBuffer = Buffer.from(roomUuid, 'hex');

    const roomInfo = await Room.findOne({ uuid: roomUuidBuffer });
    if (!_.find(roomInfo.users, d => d.toString('hex') === auth.uuid.toString('hex'))) return {
      result: null,
      errCode: 'INVALID_TOKEN'
    };

    const usersArr = await User.find({ uuid: { $in: roomInfo.users } });

    await Chat.updateMany(
      { roomUuid: roomInfo.uuid, read: { $not: { $elemMatch: { $eq: auth.uuid } } } },
      { $push: { read: auth.uuid } }
    );

    const roomJoinLeaveInfo = await RoomJoinLeave.findOne(
      { userUuid: auth.uuid, roomUuid: roomInfo.uuid },
      { joinDate: 1 }
    );
    if (!roomJoinLeaveInfo) return { result: null, errCode: 'SERVER_ERROR' };
    const { joinDate } = roomJoinLeaveInfo;

    const chatArr = await Chat.find({ roomUuid: roomInfo.uuid, sendDate: { $gte: joinDate } }).sort({ sendDate: -1 }).limit(100);

    const roomUserInfoArr = await User.find(
      { rooms: { $elemMatch: { $eq: roomInfo.uuid } } },
      { uuid: 1, nickname: 1, profileImage: 1, profileText: 1 }
    );
    const roomUserUuidStrArr = roomUserInfoArr.map(d => d.uuid.toString('hex'));
    const currentUserInfoArr = usersArr.filter(d => roomUserUuidStrArr.indexOf(d.uuid.toString('hex')) > -1);
    const leaveUserInfoArr = usersArr.filter(d => roomUserUuidStrArr.indexOf(d.uuid.toString('hex')) < 0);

    const roomNameInfo = await RoomName.findOne({ userUuid: auth.uuid, roomUuid: roomUuidBuffer }, { roomName: 1 });
    const roomName = roomNameInfo?.roomName;

    const result: GetRoomInfoResult = {
      result: {
        uuid: roomInfo.uuid.toString('hex'),
        type: roomInfo.type,
        userUuid: roomInfo.userUuid?.toString('hex') || null,
        originName: roomInfo.name,
        name: roomName || roomInfo.name || (
          roomInfo.type === 'personal' ?
          _.uniqWith(currentUserInfoArr.concat(leaveUserInfoArr), _.isEqual).filter(d => d.uuid.toString('hex') !== auth.uuid.toString('hex'))[0]?.nickname || '(Unknown)' :
          currentUserInfoArr.map(d => d.nickname).join(', ')
        ),
        maxUser: roomInfo.maxUser,
        isPrivate: roomInfo.isPrivate,
        password: roomInfo.password,
        users: roomUserInfoArr.map(d => ({
          uuid: d.uuid.toString('hex'),
          nickname: d.nickname,
          profileImage: d.profileImage,
          profileText: d.profileText
        })),
        leaveUsers: leaveUserInfoArr.map(d => ({
          uuid: d.uuid.toString('hex'),
          nickname: d.nickname,
          profileImage: d.profileImage,
          profileText: d.profileText
        })),
        createDate: roomInfo.createDate.toISOString(),
        blackList: roomInfo.blackList?.map(d => d.toString('hex')),
        messages: chatArr.map(d1 => ({
          uuid: d1.uuid.toString('hex'),
          userUuid: d1.userUuid.toString('hex'),
          roomUuid: d1.roomUuid.toString('hex'),
          type: d1.type,
          contents: ['join', 'leave'].indexOf(d1.type) < 0 ? d1.contents : _.find(usersArr, d2 => d1.userUuid.toString('hex') === d2.uuid.toString('hex'))?.nickname,
          filePath: d1.filePath,
          sendDate: d1.sendDate.toISOString(),
          read: d1.read ? d1.read.map(d2 => d2.toString('hex')) : null
        })).reverse(),
        userNum: roomUserInfoArr.length
      },
      errCode: null
    };

    return result;
  }, ctx)
);

/** [Query] 메시지 추가 조회 */
const getMessages = async (parent: any, args: GetMessagesArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetMessagesResult> => {
    const { auth, models: { User, Room, RoomJoinLeave, Chat } } = ctx;
    const { roomUuid, lastChatDate } = args;
    const roomUuidBuffer = Buffer.from(roomUuid, 'hex');

    const roomInfo = await Room.findOne({ uuid: roomUuidBuffer });
    if (!_.find(roomInfo.users, d => d.toString('hex') === auth.uuid.toString('hex'))) return {
      result: null,
      errCode: 'INVALID_TOKEN'
    };

    const usersArr = await User.find({ uuid: { $in: roomInfo.users } });

    const roomJoinLeaveInfo = await RoomJoinLeave.findOne(
      { userUuid: auth.uuid, roomUuid: roomInfo.uuid },
      { joinDate: 1 }
    );
    if (!roomJoinLeaveInfo) return { result: null, errCode: 'SERVER_ERROR' };
    const { joinDate } = roomJoinLeaveInfo;

    const chatArr = await Chat.find({
      roomUuid: roomInfo.uuid,
      sendDate: { $lt: new Date(lastChatDate), $gte: joinDate }
    }).sort({ sendDate: -1 }).limit(100);

    const result: GetMessagesResult = {
      result: chatArr.map(d => ({
        uuid: d.uuid.toString('hex'),
        userUuid: d.userUuid.toString('hex'),
        roomUuid: d.roomUuid.toString('hex'),
        type: d.type,
        contents: ['join', 'leave'].indexOf(d.type) < 0 ? d.contents : _.find(usersArr, d => d.uuid.toString('hex') === auth.uuid.toString('hex')).nickname,
        filePath: d.filePath,
        sendDate: d.sendDate.toISOString(),
        read: d.read ? d.read.map(d => d.toString('hex')) : null
      })).reverse(),
      errCode: null
    };
    return result;
  }, ctx)
);

/** [Query] 방 목록 조회 */
const getRoomList = async (parent: any, args: any, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetRoomListResult> => {
    const { auth, models: { User, Room, RoomJoinLeave, RoomName, Chat } } = ctx;

    const userInfo = await User.findOne({ uuid: auth.uuid }, { rooms: 1 });
    const roomUuidArr = userInfo.rooms;

    const roomsArr = await Room.find({ uuid: { $in: roomUuidArr }, lastChatUuid: { $ne: null } });

    let tmpUserUuidArr: Buffer[] = [];
    roomsArr.forEach(d => tmpUserUuidArr = tmpUserUuidArr.concat(d.users));
    const usersUuidArr = Array.from(new Set(tmpUserUuidArr));
    const usersArr = await User.find({ uuid: { $in: usersUuidArr } });

    const joinLeaveInfo = await RoomJoinLeave.find({ userUuid: auth.uuid }, { roomUuid: 1, joinDate: 1 });

    const lastChatUuidArr = roomsArr.map(d => d.lastChatUuid).filter(d => d !== null);
    const lastChatArr = await Chat.find({ uuid: { $in: lastChatUuidArr } });

    const allRoomUsers = await User.find({ rooms: { $elemMatch: { $in: roomUuidArr } } });

    const roomNameInfoArr = await RoomName.find({ userUuid: auth.uuid, roomUuid: { $in: roomUuidArr } });

    const roomList = roomsArr.map(d1 => {
      const roomUsers = allRoomUsers.filter(d2 => !!_.find(d2.rooms, d3 => d3.toString('hex') === d1.uuid.toString('hex')));

      return {
        uuid: d1.uuid.toString('hex'),
        userUuid: d1.userUuid ? d1.userUuid.toString('hex') : null,
        type: d1.type,
        name: (
          _.find(roomNameInfoArr, d2 => d1.uuid.toString('hex') === d2.roomUuid.toString('hex'))?.roomName ||
          d1.name || (
            d1.type === 'personal' ?
              _.find(roomUsers, d2 => d2.uuid.toString('hex') !== auth.uuid.toString('hex'))?.nickname || '(Unknown)' :
              roomUsers.map(d2 => d2.nickname).join(', ')
          )
        ),
        users: d1.type === 'personal' ? d1.users.slice(0, 4).map(d2 => {
          const tmpUserInfo = _.find(usersArr, { uuid: d2 });
          return {
            uuid: tmpUserInfo?.uuid.toString('hex'),
            nickname: tmpUserInfo?.nickname,
            profileImage: tmpUserInfo?.profileImage
          };
        }) : roomUsers.slice(0, 4).map(d3 => {
          const tmpUserInfo = _.find(usersArr, { uuid: d3.uuid });
          return {
            uuid: tmpUserInfo.uuid.toString('hex'),
            nickname: tmpUserInfo.nickname,
            profileImage: tmpUserInfo.profileImage
          };
        }),
        maxUser: d1.maxUser,
        isPrivate: d1.isPrivate,
        password: d1.password,
        createDate: d1.createDate.toISOString(),
        joinDate: _.find(joinLeaveInfo, d2 => d1.uuid.toString('hex') === d2.roomUuid.toString('hex'))?.joinDate.toISOString(),
        lastChat: (() => {
          const tmpLastChat = _.find(lastChatArr, { uuid: d1.lastChatUuid });
          return {
            uuid: tmpLastChat?.uuid.toString('hex'),
            userUuid: tmpLastChat?.userUuid.toString('hex'),
            roomUuid: tmpLastChat?.roomUuid.toString('hex'),
            type: tmpLastChat?.type,
            contents: tmpLastChat?.contents,
            filePath: tmpLastChat?.filePath,
            sendDate: tmpLastChat?.sendDate.toISOString(),
            read: tmpLastChat?.read.map(d2 => d2.toString('hex'))
          };
        })()
      }
    }).sort((o1, o2) => {
      if (o1.lastChat.sendDate > o2.lastChat.sendDate) return -1;
      else if (o1.lastChat.sendDate < o2.lastChat.sendDate) return 1;
      else return 0;
    });

    const chatsArr = await Chat.find(
      { roomUuid: { $in: roomsArr.map(d => d.uuid) }, read: { $not: { $elemMatch: { $eq: auth.uuid } } } },
      { roomUuid: 1 }
    );

    const noReadNum: { [key: string]: number } = {};
    roomsArr.map(d => d.uuid.toString('hex')).forEach(d => noReadNum[d] = 0);
    chatsArr.forEach(d => {
      const roomUuidStr = d.roomUuid.toString('hex');
      noReadNum[roomUuidStr]++;
    });

    const result: GetRoomListResult = {
      result: { roomList, noReadNumJsonString: JSON.stringify(noReadNum) },
      errCode: null
    };

    return result;
  }, ctx)
);

/** [Mutation] 채팅방 나가기, 추방하기 */
const leaveRoom = async (parent: any, args: LeaveRoomArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<LeaveRoomResult> => {
    const { auth, io, models: { User, Room, RoomJoinLeave, RoomName, Chat } } = ctx;
    const { roomUuid, userUuid, blackList } = args;

    const userUuidBuffer = !!userUuid ? Buffer.from(userUuid, 'hex') : auth.uuid;
    const roomUuidBuffer = Buffer.from(roomUuid, 'hex');

    const roomInfo = await Room.findOne({ uuid: roomUuidBuffer });
    if (
      !roomInfo ||
      (!!userUuid && !blackList) ||
      (!!userUuid && roomInfo.type !== 'open') ||
      (!!userUuid && roomInfo.userUuid.toString('hex') !== auth.uuid.toString('hex'))
    ) return {
      result: null,
      errCode: 'SERVER_ERROR'
    };

    await User.updateOne({ uuid: userUuidBuffer }, { $pull: { rooms: roomUuidBuffer } });
    const roomUsersArr = await User.find({ rooms: { $elemMatch: { $eq: roomUuidBuffer } } });
    const roomUserCount = roomUsersArr.length;

    const socket = findUserSocket(io, userUuidBuffer);

    if (roomInfo.type === 'personal') {
      if (roomUserCount === 0) {
        await Chat.deleteMany({ roomUuid: roomUuidBuffer });
      }
      if (socket) socket.emit('reset-page-container');
    } else {
      const [chatInfo] = await Chat.insertMany({
        userUuid: userUuidBuffer,
        roomUuid: roomUuidBuffer,
        type: 'leave',
        sendDate: new Date(),
        read: roomInfo.users
      });

      if (socket) {
        await socket.leave(roomUuid);
        socket.emit('reset-page-container');
      }

      if (roomUserCount === 0) {
        await Chat.deleteMany({ roomUuid: roomUuidBuffer });
        await RoomJoinLeave.deleteMany({ roomUuid: roomUuidBuffer });
        await Room.deleteMany({ uuid: roomUuidBuffer });
      } else {
        if (roomInfo.type === 'open') {
          const secondUserUuid = _.find(roomUsersArr.map(d => d.uuid), d => d.toString('hex') !== userUuidBuffer.toString('hex'));
          await Room.updateOne({ uuid: roomUuidBuffer }, { $set: { userUuid: secondUserUuid } });
          io.to(roomUuid).emit('delegate-room-master', roomUuid, secondUserUuid.toString('hex'));
        }

        await RoomName.deleteOne({ userUuid: userUuidBuffer, roomUuid: roomUuidBuffer });

        if (blackList) await Room.updateOne(
          { uuid: roomUuidBuffer },
          { $push: { blackList: !!userUuid ? Buffer.from(userUuid, 'hex') : userUuidBuffer } }
        );

        const userInfo = await User.findOne({ uuid: userUuidBuffer }, { nickname: 1 });
        const roomName = roomInfo.name || roomUsersArr.map(d => d.nickname).join(', ');

        io.to(roomUuid).emit('receive-msg', {
          uuid: chatInfo.uuid.toString('hex'),
          userUuid: chatInfo.userUuid.toString('hex'),
          roomUuid: chatInfo.roomUuid.toString('hex'),
          type: chatInfo.type,
          contents: userInfo?.nickname,
          filePath: chatInfo.filePath,
          sendDate: chatInfo.sendDate.toISOString(),
          read: chatInfo.read.map(d => d.toString('hex'))
        } as ChatInfo, blackList ? userInfo : undefined, roomName);
      }

      io.to(roomUuid).emit('refresh-rooms');
    }

    if (roomUserCount !== 0) await RoomJoinLeave.updateOne(
      { userUuid: userUuidBuffer, roomUuid: roomUuidBuffer },
      { $set: { joinDate: null, leaveDate: new Date() } }
    );

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 그룹/오픈 채팅방 참가 */
const enterRoom = async (parent: any, args: EnterRoomArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<EnterRoomResult> => {
    const { auth, io, models: { User, Room, RoomJoinLeave, Chat } } = ctx;
    const { type, roomUuid, userUuid, password } = args;
    const roomUuidBuffer = Buffer.from(roomUuid, 'hex');
    const userUuidBufferArr = userUuid.map(d => Buffer.from(d, 'hex'));

    const roomInfo = await Room.findOne({ uuid: roomUuidBuffer });
    const roomUserInfoArr = await User.find({ rooms: { $elemMatch: { $eq: roomUuidBuffer } } });

    const isInvalidRoom = !roomInfo || roomInfo.type === 'personal' || roomInfo.type !== type;
    const isExistUser = !!_.find(roomUserInfoArr, d => d.uuid.toString('hex') === auth.uuid.toString('hex'));
    const isBlackListUser = !!_.find(roomInfo.blackList, d => userUuid.indexOf(d.toString('hex')) > -1);

    if (isInvalidRoom || isBlackListUser) return {
      result: null,
      errCode: 'SERVER_ERROR'
    };

    if (!isExistUser && userUuid.indexOf(auth.uuid.toString('hex')) < 0) return {
      result: null,
      errCode: 'INVALID_USER'
    };
    console.log({ isInvalidRoom, isExistUser, isBlackListUser });

    if (roomInfo.type === 'open') {
      if (roomUserInfoArr.length === roomInfo.maxUser) return {
        result: null,
        errCode: 'MAX_USER'
      };

      if (!!roomInfo.password) {
        const encodedPassword = SHA256(password).toString(enc.Hex);

        if (!password || (roomInfo.password !== encodedPassword)) {
          return {
            result: null,
            errCode: 'WRONG_PASSWORD'
          };
        }
      }
    }

    const userInfoArr = await User.find({ uuid: { $in: userUuidBufferArr } });

    for (const userInfo of userInfoArr) {
      if (!userInfo || !!_.find(userInfo.rooms, d => d.toString('hex') === roomUuid)) return {
        result: null,
        errCode: 'INVALID_USER'
      };
    }

    const joinDate = new Date();
    const chatMessageArr: ChatInfo[] = [];
    
    const newCahtArr: MongooseModels.ChatModel[] = [];
    const updateUserUuidArr: Buffer[] = [];
    const newRoomUsersArr: Buffer[] = [...roomInfo.users];

    const readUserUuidArr = Array.from(new Set(roomInfo.users.map(d => d.toString('hex')).concat(userUuid))).map(d => Buffer.from(d, 'hex'));

    for (const userInfo of userInfoArr) {
      const userUuidBuffer = userInfo.uuid;

      const chatInfo: MongooseModels.ChatModel = {
        uuid: generateUuidBuffer(),
        userUuid: userUuidBuffer,
        roomUuid: roomUuidBuffer,
        type: 'join',
        contents: null,
        filePath: '',
        sendDate: joinDate,
        read: readUserUuidArr
      };
      newCahtArr.push(chatInfo);

      chatMessageArr.push({
        uuid: chatInfo.uuid.toString('hex'),
        userUuid: chatInfo.userUuid.toString('hex'),
        roomUuid: chatInfo.roomUuid.toString('hex'),
        type: chatInfo.type,
        contents: userInfo?.nickname,
        filePath: chatInfo.filePath,
        sendDate: chatInfo.sendDate.toISOString(),
        read: readUserUuidArr.map(d => d.toString('hex'))
      } as ChatInfo);

      updateUserUuidArr.push(userUuidBuffer);

      if (!_.find(roomInfo.users, d => d.toString('hex') === userUuidBuffer.toString('hex') )) {
        newRoomUsersArr.push(userUuidBuffer);
      }

      const socket = findUserSocket(io, userUuidBuffer);
      if (socket) await socket.join(roomUuid);
    }

    await Chat.insertMany(newCahtArr);
    await User.updateMany({ uuid: { $in: updateUserUuidArr } }, { $push: { rooms: roomUuidBuffer } });
    await Room.updateOne({ uuid: roomUuidBuffer }, { $set: { users: newRoomUsersArr } });
    await RoomJoinLeave.deleteMany({ userUuid: { $in: updateUserUuidArr }, roomUuid: roomUuidBuffer });
    await RoomJoinLeave.insertMany(updateUserUuidArr.map(d => ({
      userUuid: d,
      roomUuid: roomUuidBuffer,
      joinDate
    })));

    io.to(roomUuid).emit('refresh-rooms');

    const newRoomUserInfoArr = await User.find({ uuid: { $in: newRoomUsersArr } }, { nickname: 1 });
    const roomName = roomInfo.name || newRoomUserInfoArr.map(d => d.nickname).join(', ') || '(Unknown)';

    for (const chatMessage of chatMessageArr) {
      const newUserInfo = _.find(userInfoArr, d => chatMessage.userUuid === d.uuid.toString('hex'));

      io.to(roomUuid).emit('receive-msg', chatMessage, {
        uuid: newUserInfo.uuid.toString('hex'),
        nickname: newUserInfo.nickname,
        profileImage: newUserInfo.profileImage,
        profileText: newUserInfo.profileText
      } as Partial<UserInfo>, roomName);
    }

    return {
      result: roomName,
      errCode: null
    };
  }, ctx)
);

/** [Query] 오픈 채팅방 목록 조회 */
const getOpenChatRoomList = async (parent: any, args: GetOpenChatRoomArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetOpenChatRoomResult> => {
    const { auth, models: { User, Room, Chat } } = ctx;
    let { roomName, page } = args;
    if (!page) page = 1;

    const userInfo = await User.findOne({ uuid: auth.uuid }, { rooms: 1 });
    if (!userInfo) return {
      result: null,
      errCode: 'SERVER_ERROR'
    };

    const roomUuidArr = userInfo.rooms;
    const openChatRoomList = await Room.find({
      uuid: { $nin: roomUuidArr },
      type: 'open',
      name: RegExp(roomName),
      isPrivate: false,
      blackList: { $not: { $elemMatch: { $eq: auth.uuid } } }
    }).skip((page - 1) * 50).limit(50);

    let usersUuidArr: Buffer[] = [];
    openChatRoomList.forEach(d => usersUuidArr = usersUuidArr.concat(d.users));
    usersUuidArr = Array.from(new Set(usersUuidArr));
    const usersArr = await User.find({ uuid: { $in: usersUuidArr } });

    const lastChatUuidArr = openChatRoomList.map(d => d.lastChatUuid);
    const lastChatArr = await Chat.find({ uuid: { $in: lastChatUuidArr } });

    const result: RoomListItem[] = openChatRoomList.map<RoomListItem>(d1 => ({
      uuid: d1.uuid.toString('hex'),
      userUuid: d1.userUuid.toString('hex'),
      type: d1.type,
      name: d1.name,
      users: (() => {
        let result: Partial<UserInfo>[] = [];
        for (const u of usersArr) {
          if (!_.find(u.rooms, d2 => d1.uuid.toString('hex') === d2.toString('hex'))) continue;
          result.push({
            uuid: u.uuid.toString('hex'),
            profileImage: u.profileImage
          });
          if (result.length >= 4) break;
        }
        return result;
      })(),
      maxUser: d1.maxUser,
      isPrivate: d1.isPrivate,
      password: d1.password,
      createDate: d1.createDate.toISOString(),
      joinDate: null,
      lastChat: (() => {
        const tmpLastChat = _.find(lastChatArr, d2 => d1.lastChatUuid.toString('hex') === d2.uuid.toString('hex'));
        return {
          uuid: tmpLastChat?.uuid.toString('hex'),
          userUuid: tmpLastChat?.userUuid.toString('hex'),
          roomUuid: tmpLastChat?.roomUuid.toString('hex'),
          type: tmpLastChat?.type,
          contents: null,
          filePath: null,
          sendDate: tmpLastChat?.sendDate.toISOString(),
          read: []
        };
      })(),
      userNum: _.filter(usersArr, d2 => d2.rooms.map(d3 => d3.toString('hex')).indexOf(d1.uuid.toString('hex')) > -1).length
    }));

    return {
      result,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 유저별 채팅방 이름 변경 */
const renameChatRoom = async (parent: any, args: RenameChatRoomArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<RenameChatRoomResult> => {
    const { io, auth, models: { User, Room, RoomName } } = ctx;
    const { roomUuid, roomName } = args;
    const roomUuidBuffer = Buffer.from(roomUuid, 'hex');
    console.log(args);

    const roomInfo = await Room.findOne({ uuid: roomUuidBuffer }, { type: 1, name: 1 });
    if (!roomInfo /*|| roomInfo.type !== 'group'*/) return {
      result: null,
      errCode: 'SERVER_ERROR'
    };

    const isExistRoomNameData = !!(await RoomName.countDocuments({ userUuid: auth.uuid, roomUuid: roomUuidBuffer }));

    if (!!roomName) {
      if (isExistRoomNameData) {
        await RoomName.updateOne(
          { userUuid: auth.uuid, roomUuid: roomUuidBuffer },
          { $set: { roomName } }
        );
      } else {
        if (roomInfo.name !== roomName) {
          await RoomName.insertMany({
            userUuid: auth.uuid,
            roomUuid: roomUuidBuffer,
            roomName
          });
        }
      }
    } else {
      await RoomName.deleteOne({ userUuid: auth.uuid, roomUuid: roomUuidBuffer });
    }

    const socket = findUserSocket(io, auth.uuid);
    socket.emit('refresh-rooms');

    return {
      result: roomName || roomInfo.name || (
        (await User.find({ rooms: { $elemMatch: { $eq: roomUuidBuffer } } }))
          .filter(d => roomInfo.type === 'personal' ? d.uuid.toString('hex') !== auth.uuid.toString('hex') : true)
          .map(d => d.nickname).join(', ')
      ),
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 오픈 채팅방 방장 위임 */
const delegateRoomMaster = async (parent: any, args: DelegateRoomMasterArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<DelegateRoomMasterResult> => {
    const { io, auth, models: { User, Room } } = ctx;
    const { roomUuid, userUuid } = args;
    const roomUuidBuffer = Buffer.from(roomUuid, 'hex');
    const userUuidBuffer = Buffer.from(userUuid, 'hex');

    const userInfoArr = await User.find({ rooms: { $elemMatch: { $eq: roomUuidBuffer } } });
    const userUuidArr = userInfoArr.map(d => d.uuid.toString('hex'));
    const roomInfo = await Room.findOne({ uuid: roomUuidBuffer });

    if (roomInfo.type !== 'open') return {
      result: null,
      errCode: 'SERVER_ERROR'
    };

    if (roomInfo.userUuid.toString('hex') !== auth.uuid.toString('hex')) return {
      result: null,
      errCode: 'INVALID_USER'
    };

    if (userUuidArr.indexOf(userUuid) < 0) return {
      result: null,
      errCode: 'NO_USER'
    };

    await Room.updateOne({ uuid: roomUuidBuffer }, { $set: { userUuid: userUuidBuffer } });

    io.to(roomUuid).emit('delegate-room-master', roomUuid, userUuid);

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 오픈 채팅방 설정 변경 */
const changeRoomSettings = async (parent: any, args: ChangeRoomSettingsArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<ChangeRoomSettingsResult> => {
    const { io, auth, models: { User, Room } } = ctx;
    const { roomUuid } = args;
    const roomUuidBuffer = Buffer.from(roomUuid, 'hex');
    const newRoomInfo = _.cloneDeep(args);
    delete newRoomInfo.roomUuid;

    const roomUsersArr = await User.find({ rooms: { $elemMatch: { $eq: roomUuidBuffer } } });

    const roomInfo = await Room.findOne({ uuid: roomUuidBuffer });
    if (!roomInfo || roomInfo.userUuid.toString('hex') !== auth.uuid.toString('hex') || !newRoomInfo.name) return {
      result: null,
      errCode: 'SERVER_ERROR'
    };
    const prevRoomName = roomInfo.name;

    if (newRoomInfo.maxUser < roomUsersArr.length) return {
      result: null,
      errCode: 'INVALID_MAX_USER'
    };

    const isExistRoomName = !!(await Room.countDocuments({
      uuid: { $ne: roomUuidBuffer },
      name: newRoomInfo.name
    }));
    if (isExistRoomName) return {
      result: null,
      errCode: 'DUPLICATED_ROOM_NAME'
    };

    if (newRoomInfo.password !== roomInfo.password) {
      if (!!newRoomInfo.password) newRoomInfo.password = SHA256(newRoomInfo.password).toString(enc.Hex);
      else newRoomInfo.password = null;
    }

    await Room.updateOne({ uuid: roomUuidBuffer }, { $set: newRoomInfo });

    newRoomInfo.roomUuid = roomUuid;
    io.to(roomUuid).emit('change-room-settings', newRoomInfo, prevRoomName);

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 사진, 동영상, 오디오, 파일 채팅 메시지 전송 */
const sendUploadChat = async (parent: any, args: SendUploadChatArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<SendUploadChatResult> => {
    const { io, auth, models: { Room, Chat } } = ctx;
    const { roomUuid, type, files, compress } = args;
    const roomUuidBuffer = Buffer.from(roomUuid, 'hex');

    const uploadDirPath = path.join(UPLOAD_PATH, auth.uuid.toString('hex'), moment().format('YYYYMMDD'));
    if (!fs.existsSync(uploadDirPath)) fs.mkdirSync(uploadDirPath, { recursive: true });

    const isMediaType = ['image', 'video', 'audio'].indexOf(type) > -1;
    const tmp: { createReadStream: () => fs.ReadStream, filename: string }[] = [];

    for (const file of files) {
      const { createReadStream, filename, mimetype } = await file;
      if (isMediaType && mimetype.split('/')[0] !== type) return {
        result: null,
        errCode: 'INVALID_FILE_TYPE'
      };
      console.log({ filename, mimetype });
      tmp.push({ createReadStream, filename });
    }

    let compressed = false;
    let filePaths = [];
    let size = 0;

    const socket = findUserSocket(io, auth.uuid);

    if (compress && files.length > 1) {
      const filePath = path.join(uploadDirPath, `${Date.now()}_download.zip`);
      filePaths.push(filePath);
      const writeSteam = fs.createWriteStream(filePath);

      const promise = new Promise((resolve, reject) => {
        const archive = archiver('zip', { zlib: { level: 9 } });
        archive.on('error', err => reject(err));
        archive.pipe(writeSteam);

        for (const { createReadStream, filename } of tmp) {
          const readStream = createReadStream();
          archive.append(readStream, { name: filename });

          readStream.on('data', chunk => {
            size += chunk.length;
            if (socket) socket.emit('uploading', size);
          });
        }

        archive.finalize().then(() => resolve(true)).catch(err => reject(err));
      });
      await promise;
      compressed = true;
    } else {
      const finishedArr: Promise<boolean>[] = [];
      for (const { createReadStream, filename } of tmp) {
        const filePath = path.join(uploadDirPath, `${Date.now()}_${filename}`);

        const readStream = createReadStream();

        readStream.on('data', chunk => {
          size += chunk.length;
          if (socket) socket.emit('uploading', size);
        });

        const writeSteam = fs.createWriteStream(filePath);
        readStream.pipe(writeSteam);

        finishedArr.push(finishedAsync(writeSteam));
        filePaths.push(filePath);
      }
      await Promise.all(finishedArr);
    }

    const chatArr = [];

    for (const filePath of filePaths) {
      chatArr.push({
        userUuid: auth.uuid,
        roomUuid: roomUuidBuffer,
        type: compressed ? 'file' : type,
        contents: path.basename(filePath),
        filePath: filePath.replace(appRoot, ''),
        read: [auth.uuid]
      });
    }

    const chatInfoArr = await Chat.insertMany(chatArr);
    await Room.updateOne({ uuid: roomUuidBuffer }, { $set: { lastChatUuid: chatInfoArr[chatInfoArr.length - 1].uuid } })

    for (const chatInfo of chatInfoArr) {
      io.to(roomUuid).emit('receive-msg', {
        uuid: chatInfo.uuid.toString('hex'),
        userUuid: chatInfo.userUuid.toString('hex'),
        roomUuid: chatInfo.roomUuid.toString('hex'),
        type: chatInfo.type,
        contents: chatInfo.contents,
        filePath: chatInfo.filePath,
        read: chatInfo.read.map(d => d.toString('hex')),
        sendDate: chatInfo.sendDate.toISOString()
      } as ChatInfo);
    }

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

const resolver = {
  Query: {
    getPersonalRoomInfo,
    getRoomInfo,
    getMessages,
    getRoomList,
    getOpenChatRoomList
  },
  Mutation: {
    createRoom,
    leaveRoom,
    enterRoom,
    renameChatRoom,
    delegateRoomMaster,
    changeRoomSettings,
    sendUploadChat
  }
};

export default resolver;