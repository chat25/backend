import createRoom from './typeDefs/mutation/createRoom.gql';
import getPersonalRoomInfo from './typeDefs/query/getPersonalRoomInfo.gql';
import getRoomInfo from './typeDefs/query/getRoomInfo.gql';
import getMessages from './typeDefs/query/getMessages.gql';
import getRoomList from './typeDefs/query/getRoomList.gql';
import leaveRoom from './typeDefs/mutation/leaveRoom.gql';
import enterRoom from './typeDefs/mutation/enterRoom.gql';
import getOpenChatRoomList from './typeDefs/query/getOpenChatRoomList.gql';
import renameChatRoom from './typeDefs/mutation/renameChatRoom.gql';
import delegateRoomMaster from './typeDefs/mutation/delegateRoomMaster.gql';
import changeRoomSettings from './typeDefs/mutation/changeRoomSettings.gql';
import sendUploadChat from './typeDefs/mutation/sendUploadChat.gql';

import resolver from './resolver';

export const roomTypeDefs = [
  createRoom,
  getPersonalRoomInfo,
  getRoomInfo,
  getMessages,
  getRoomList,
  leaveRoom,
  enterRoom,
  getOpenChatRoomList,
  renameChatRoom,
  delegateRoomMaster,
  changeRoomSettings,
  sendUploadChat
];

export const roomResolver = resolver;