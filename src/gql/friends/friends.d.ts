interface Friend {
  uuid: string
  nickname: string
  profileImage: string
  profileText: string
}

interface GetFriendsInfoArgs {
  searchNickname?: string
  lastNickname?: string
}

interface GetFriendsInfoResult {
  result: {
    friendRequests: Friend[]
    friends: Friend[]
  }
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface SearchUserByNicknameArgs {
  nickname: string
  lastNickname?: string
}

interface SearchUserByNicknameResult {
  result: Friend[]
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface FriendRequestArgs {
  requestUserUuid: string
}

interface FriendRequestResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'ALREADY_FRIEND' | 'ALREADY_REQUESTED' | 'SERVER_ERROR'
}

interface FriendResponseArgs {
  requestUserUuid: string
  isAccept: boolean
}

interface FriendResponseResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}

interface DeleteFriendArgs {
  friendUuid: string
}

interface DeleteFriendResult {
  result: boolean
  errCode: 'INVALID_TOKEN' | 'SERVER_ERROR'
}