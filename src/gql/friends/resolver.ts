import { generateUuidBuffer, gqlResolver } from '../../common/utils';

/** [Query] 친구/친구요청 목록 조회 */
const getFriendsInfo = async (parent: any, args: GetFriendsInfoArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetFriendsInfoResult> => {
    const { auth, models: { User, FriendRequest } } = ctx;
    const { searchNickname, lastNickname } = args;

    let friendRequests: Friend[] = null;

    if (!searchNickname) {
      const friendRequestsArr = await FriendRequest.find({ to: auth.uuid }, { _id: 0, __v: 0 });
      const friendRequestsData = await User.find({ uuid: { $in: friendRequestsArr.map(d => d.from) } }, {
        uuid: 1,
        nickname: 1,
        profileImage: 1,
        profileText: 1
      });
      friendRequests = friendRequestsData.map(d => ({
        ...d.toJSON(),
        uuid: d.uuid.toString('hex')
      }));
    }

    const { friends: friendsUuidArr } = await User.findOne({ uuid: auth.uuid }, { friends: 1 });

    const friendsInfoFileterQuery = !!searchNickname ? {
      $and: [
        { uuid: { $in: friendsUuidArr } },
        { nickname: { $gt: lastNickname || '' } },
        { nickname: RegExp(searchNickname) }
      ]
    } : {
      uuid: { $in: friendsUuidArr },
      nickname: { $gt: lastNickname || '' }
    };

    const friendsData = await User.find(friendsInfoFileterQuery, {
      uuid: 1,
      nickname: 1,
      profileImage: 1,
      profileText: 1
    }).sort({ nickname: 1 }).limit(50);

    const friends: Friend[] = friendsData.map(d => ({
      ...d.toJSON(),
      uuid: d.uuid.toString('hex')
    }));

    return {
      result: { friendRequests, friends },
      errCode: null
    };
  }, ctx)
);

/** [Query] 닉네임으로 유저 검색 */
const searchUserByNickname = async (parent: any, args: SearchUserByNicknameArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<SearchUserByNicknameResult> => {
    const { auth, models: { User, FriendRequest } } = ctx;
    const { nickname, lastNickname } = args;

    const { friends: friendsUuidArr } = await User.findOne({ uuid: auth.uuid }, { friends: 1 });
    const requestedUsers1 = await FriendRequest.find({ from: auth.uuid }, { to: 1 });
    const requestedUsers2 = await FriendRequest.find({ to: auth.uuid }, { from: 1 });
    const requestedUserArr = [
      ...requestedUsers1.map(d => d.to),
      ...requestedUsers2.map(d => d.from)
    ];

    const userArr = await User.find({
      $and: [
        { nickname: RegExp(nickname) },
        { nickname: { $gt: lastNickname || '' } },
        { uuid: { $nin: [auth.uuid, ...friendsUuidArr, ...requestedUserArr] } }
      ]
    }, {
      uuid: 1,
      nickname: 1,
      profileImage: 1,
      profileText: 1
    }).sort({ nickname: 1 }).limit(50);

    return {
      result: userArr.map(d => ({
        ...d.toJSON(),
        uuid: d.uuid.toString('hex')
      })),
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 친구 요청 */
const friendRequest = async (parent: any, args: FriendRequestArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<FriendRequestResult> => {
    const { requestUserUuid } = args;
    const { auth, models: { User, FriendRequest }, io } = ctx;
    const requestUserUuidBuffer = Buffer.from(requestUserUuid, 'hex');

    const isFriend = await User.countDocuments({
      uuid: auth.uuid,
      friends: { $elemMatch: { $eq: requestUserUuidBuffer } }
    });

    if (isFriend > 0) return {
      result: false,
      errCode: 'ALREADY_FRIEND'
    };

    const isRequested = await FriendRequest.countDocuments({
      from: auth.uuid,
      to: requestUserUuidBuffer
    });

    if (isRequested > 0) return {
      result: false,
      errCode: 'ALREADY_REQUESTED'
    };

    await FriendRequest.insertMany({
      from: auth.uuid,
      to: requestUserUuidBuffer
    });

    io.to(requestUserUuidBuffer.toString('hex')).emit('refresh-friends');

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 친구 수락/거절 */
const friendResponse = async (parent: any, args: FriendResponseArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<FriendResponseResult> => {
    const { auth, models: { User, FriendRequest, Room, RoomJoinLeave }, io } = ctx;
    const { requestUserUuid, isAccept } = args;
    const requestUserUuidBuffer = Buffer.from(requestUserUuid, 'hex');

    const requestInfo = await FriendRequest.findOneAndDelete({ from: requestUserUuidBuffer, to: auth.uuid });
    if (!requestInfo) return {
      result: null,
      errCode: 'INVALID_TOKEN'
    };

    if (isAccept) {
      const isExistRoom = !!(await Room.countDocuments({ users: { $in: [[auth.uuid, requestUserUuidBuffer], [requestUserUuidBuffer, auth.uuid]] } }));
      const newRoomUuid = generateUuidBuffer();
      if (!isExistRoom) {
        const newRoomInfo: MongooseModels.PersonalRoom = {
          uuid: newRoomUuid,
          type: 'personal',
          users: [auth.uuid, requestUserUuidBuffer],
          lastChatUuid: null
        };

        await Room.insertMany(newRoomInfo);
      }

      await User.updateOne({ uuid: auth.uuid }, {
        $push: isExistRoom ?
          { friends: requestUserUuidBuffer } :
          { friends: requestUserUuidBuffer, rooms: newRoomUuid }
      });
      await User.updateOne({ uuid: requestUserUuidBuffer }, {
        $push: isExistRoom ?
          { friends: auth.uuid } :
          { friends: auth.uuid, rooms: newRoomUuid }
      });

      const joinDate = new Date();
      await RoomJoinLeave.insertMany({
        userUuid: auth.uuid,
        roomUuid: newRoomUuid,
        joinDate
      });
      await RoomJoinLeave.insertMany({
        userUuid: requestUserUuidBuffer,
        roomUuid: newRoomUuid,
        joinDate
      });

      io.to(auth.uuid.toString('hex')).to(requestUserUuidBuffer.toString('hex')).emit('refresh-friends');
    }

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 친구 삭제 */
const deleteFriend = async (parent: any, args: DeleteFriendArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<DeleteFriendResult> => {
    const { auth, models: { User }, io } = ctx;
    const { friendUuid } = args;
    const friendUuidBuffer = Buffer.from(friendUuid, 'hex');

    await User.updateOne({ uuid: auth.uuid }, { $pull: { friends: friendUuidBuffer } });
    await User.updateOne({ uuid: friendUuidBuffer }, { $pull: { friends: auth.uuid } });
    io.to(friendUuidBuffer.toString('hex')).emit('refresh-friends');

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

const resolver = {
  Query: {
    getFriendsInfo,
    searchUserByNickname
  },
  Mutation: {
    friendRequest,
    friendResponse,
    deleteFriend
  }
};

export default resolver;