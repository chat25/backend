import getFriendsInfo from './typeDefs/query/getFriendsInfo.gql';
import searchUserByNickname from './typeDefs/query/searchUserByNickname.gql';
import friendRequest from './typeDefs/mutation/friendRequest.gql';
import friendResponse from './typeDefs/mutation/friendResponse.gql';
import deleteFriend from './typeDefs/mutation/deleteFriend.gql';

import resolver from './resolver';

export const friendsTypeDefs = [
  getFriendsInfo,
  searchUserByNickname,
  friendRequest,
  friendResponse,
  deleteFriend
];

export const friendsResolver = resolver;