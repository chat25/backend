import _ from 'lodash';
import { SHA256, enc } from 'crypto-js';
import { gqlResolver } from '../../common/utils';
import { commonConfig } from '../../common/config';
import { createToken } from '../../common/jwt';
import { findUserSocket } from '../../socket';

/** [Query] 관리자 로그인 */
const adminLogin = async (parent: any, args: AdminLoginArgs) => (
  await gqlResolver(async (): Promise<AdminLoginResult> => {
    const { admin } = commonConfig;
    const { adminId, password } = args;

    if (SHA256(`${adminId}${password}`).toString(enc.Hex) !== admin) return {
      result: null,
      errCode: 'INVALID_ADMIN'
    };

    const token = createToken({
      type: 'admin',
      data: { admin }
    }, '1h');

    return {
      result: token,
      errCode: null
    };
  })
)

/** [Query] 전체 유저 정보 조회 */
const getAllUsersInfo = async (parent: any, args: any, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetAllUsersInfoResult> => {
    const { models: { User, BanHistory } } = ctx;

    const userInfoArr = await User.find({}, { uuid: 1, userId: 1, nickname: 1, name: 1, countryCode: 1, phone: 1, email: 1 });
    const bannedInfoArr = await BanHistory.find({ $and: [{ manualUnban: false }, { unbanDate: { $gt: new Date() } }] });

    return {
      result: userInfoArr.map(d1 => ({
        userUuid: d1.uuid.toString('hex'),
        userId: d1.userId,
        nickname: d1.nickname,
        name: d1.name,
        phone: `+${d1.countryCode.replace('+', '')} ${d1.phone}`,
        email: d1.email,
        isBan: !!_.find(bannedInfoArr, d2 => d2.userUuid.toString('hex') === d1.uuid.toString('hex'))
      })),
      errCode: null
    };
  }, ctx)
);

/** [Query] 유저 상세 정보 조회 */
const getUserDetailInfo = async (parent: any, args: GetUserDetailInfoArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetUserDetailInfoResult> => {
    const { io, models: { User, BanHistory } } = ctx;
    const { userUuid } = args;
    const userUuidBuffer = Buffer.from(userUuid, 'hex');

    const userInfo = await User.findOne({ uuid: userUuidBuffer });
    if (!userInfo) return {
      result: null,
      errCode: 'SERVER_ERROR'
    };

    const bannedInfoArr = await BanHistory.find({ $and: [{ userUuid: userUuidBuffer }, { manualUnban: false }, { unbanDate: { $gt: new Date() } }] });
    const connectionStatus = !!findUserSocket(io, userUuidBuffer);

    return {
      result: {
        uuid: userInfo.uuid.toString('hex'),
        userId: userInfo.userId,
        nickname: userInfo.nickname,
        name: userInfo.name,
        phone: `+${userInfo.countryCode.replace('+', '')} ${userInfo.phone}`,
        email: userInfo.email,
        birth: userInfo.birth.toISOString(),
        profileImage: userInfo.profileImage,
        profileText: userInfo.profileText,
        registDate: userInfo.registDate.toISOString(),
        isBan: bannedInfoArr.length > 0,
        connectionStatus
      },
      errCode: null
    }
  }, ctx)
);

/** [Mutation] 유저 정지 */
const banUsers = async (parent: any, args: BanUsersArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<BanUsersResult> => {
    const { io, models: { BanHistory } } = ctx;
    const { userUuid, banDay } = args;

    const banDate = new Date();
    const unbanDate = new Date();
    unbanDate.setDate(unbanDate.getDate() + banDay);

    await BanHistory.insertMany(userUuid.map(d => ({
      userUuid: Buffer.from(d, 'hex'),
      banDate,
      unbanDate,
      updateDate: banDate
    })));

    userUuid.forEach(d => io.to(d).emit('ban', banDate.toISOString(), unbanDate.toISOString()));

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 유저 정지 해제 */
const unbanUsers = async (parent: any, args: UnbanUsersArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<UnbanUsersResult> => {
    const { models: { BanHistory } } = ctx;
    const { userUuid } = args;
    const userUuidArr = userUuid.map(d => Buffer.from(d, 'hex'));

    await BanHistory.updateMany({
      $and: [
        { userUuid: { $in: userUuidArr } },
        { manualUnban: false },
        { unbanDate: { $gt: new Date() } }
      ]
    }, {
      $set: { manualUnban: true }
    });

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Query] 유저 정지 내역 조회 */
const getBanHistories = async (parent: any, args: GetBanHistoriesArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetBanHistoriesResult> => {
    const { models: { User, BanHistory } } = ctx;
    const { page, pageSize } = args;

    const totalBanHistoriesCount = await BanHistory.countDocuments({});
    const banHistories = await BanHistory.find({}).sort({ updateDate: -1 }).skip(page * pageSize).limit(pageSize);
    const bannedUserUuidArr: Buffer[] = _.uniqWith(banHistories.map(d => d.userUuid), (d1, d2) => d1.toString('hex') === d2.toString('hex'));
    const bannedUserArr = await User.find({ uuid: { $in: bannedUserUuidArr } });
    const nowDate = new Date();

    const banHistoriesData: BanHistoriesData[] = banHistories.map(d1 => {
      const userInfo = _.find(bannedUserArr, d2 => d1.userUuid.toString('hex') === d2.uuid.toString('hex'));
      return {
        uuid: d1.uuid.toString('hex'),
        userUuid: userInfo?.uuid.toString('hex') || '(Unknown)',
        userId: userInfo?.userId || '(Unknown)',
        nickname: userInfo?.nickname || '(Unknown)',
        name: userInfo?.name || '(Unknown)',
        phone: !!userInfo ? `+${userInfo.countryCode.replace('+', '')} ${userInfo.phone}` : '(Unknown)',
        email: userInfo?.email || '(Unknown)',
        isBan: d1.unbanDate > nowDate && !d1.manualUnban,
        banDate: d1.banDate.toISOString(),
        unbanDate: d1.unbanDate.toISOString(),
        manualUnban: d1.manualUnban,
        updateDate: d1.updateDate.toISOString()
      };
    });

    return {
      result: {
        rowCount: totalBanHistoriesCount,
        page,
        pageSize,
        data: banHistoriesData
      },
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 공지 전송 */
const sendNotice = async (parent: any, args: SendNoticeArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<SendNoticeResult> => {
    const { io, models: { NoticeHistory } } = ctx;
    const { userUuid, noticeDate, noticeTitle, noticeContents, sendImmediately, forceSignout } = args;
    const userUuidArr = userUuid.map(d => Buffer.from(d, 'hex'));

    const registDate = (new Date()).toISOString();

    const noticeInfoArr = await NoticeHistory.insertMany(userUuidArr.map(d => ({
      userUuid: d,
      noticeTitle: noticeTitle.ko,
      noticeTitleEn: noticeTitle.en,
      noticeContents: noticeContents.ko,
      noticeContentsEn: noticeContents.en,
      noticeStartDate: noticeDate.start,
      noticeEndDate: noticeDate.end,
      registDate
    })));

    console.log(noticeInfoArr);
    if (sendImmediately) userUuid.forEach(d => io.to(d).emit('notice', forceSignout));

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Query] 공지 내역 조회 */
const getNoticeHistories = async (parent: any, args: GetNoticeHistoriesArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetNoticeHistoriesResult> => {
    const { models: { User, NoticeHistory } } = ctx;
    const { page, pageSize } = args;

    const totalBanHistoriesCount = await NoticeHistory.countDocuments({});
    const noticeHistoriesArr = await NoticeHistory.find({}).sort({ registDate: -1 }).skip(page * pageSize).limit(pageSize);
    const userUuidArr = noticeHistoriesArr.map(d => d.userUuid);
    const userInfoArr = await User.find({ uuid: { $in: userUuidArr } });

    const noticeHistoriesData: NoticeHistoriesData[] = noticeHistoriesArr.map(d1 => {
      const userInfo = _.find(userInfoArr, d2 => d1.userUuid.toString('hex') === d2.uuid.toString('hex'));
      return {
        uuid: d1.uuid.toString('hex'),
        userUuid: userInfo?.uuid.toString('hex'),
        userId: userInfo?.userId || '(Unknown)',
        nickname: userInfo?.nickname || '(Unknown)',
        noticeTitle: d1.noticeTitle,
        noticeTitleEn: d1.noticeTitleEn,
        registDate: d1.registDate.toISOString()
      };
    });

    return {
      result: {
        rowCount: totalBanHistoriesCount,
        page,
        pageSize,
        data: noticeHistoriesData
      },
      errCode: null
    };
  }, ctx)
);

/** [Query] 공지 내역 상세 조회 */
const getNoticeDetailInfo = async (parent: any, args: GetNoticeDetailInfoArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetNoticeDetailInfoResult> => {
    const { models: { NoticeHistory } } = ctx;
    const { noticeUuid } = args;

    const noticeInfo = await NoticeHistory.findOne({ uuid: Buffer.from(noticeUuid, 'hex') });
    if (!noticeInfo) return {
      result: null,
      errCode: 'SERVER_ERROR'
    };

    return {
      result: {
        uuid: noticeInfo.uuid.toString('hex'),
        noticeTitle: noticeInfo.noticeTitle,
        noticeTitleEn: noticeInfo.noticeTitleEn,
        noticeContents: noticeInfo.noticeContents,
        noticeContentsEn: noticeInfo.noticeContentsEn,
        noticeStartDate: noticeInfo.noticeStartDate.toISOString(),
        noticeEndDate: noticeInfo.noticeEndDate.toISOString(),
        registDate: noticeInfo.registDate.toISOString(),
        read: noticeInfo.read
      },
      errCode: null
    };
  }, ctx)
);

const resolver = {
  Query: {
    adminLogin,
    getAllUsersInfo,
    getUserDetailInfo,
    getBanHistories,
    getNoticeHistories,
    getNoticeDetailInfo
  },
  Mutation: {
    banUsers,
    unbanUsers,
    sendNotice
  }
};

export default resolver;