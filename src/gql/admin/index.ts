import adminLogin from './typeDefs/query/adminLogin.gql';
import getAllUsersInfo from './typeDefs/query/getAllUsersInfo.gql';
import getUserDetailInfo from './typeDefs/query/getUserDetailInfo.gql';
import banUsers from './typeDefs/mutation/banUsers.gql';
import unbanUsers from './typeDefs/mutation/unbanUsers.gql';
import getBanHistories from './typeDefs/query/getBanHistories.gql';
import sendNotice from './typeDefs/mutation/sendNotice.gql';
import getNoticeHistories from './typeDefs/query/getNoticeHistories.gql';
import getNoticeDetailInfo from './typeDefs/query/getNoticeDetailInfo.gql';

import resolver from './resolver';

export const adminTypeDefs = [
  adminLogin,
  getAllUsersInfo,
  getUserDetailInfo,
  banUsers,
  unbanUsers,
  getBanHistories,
  sendNotice,
  getNoticeHistories,
  getNoticeDetailInfo
];

export const adminResolver = resolver;