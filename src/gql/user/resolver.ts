import Mustache from 'mustache';
import _ from 'lodash';
import { v4 as uuidV4 } from 'uuid';
import { enc, SHA256 } from 'crypto-js';
import { createToken } from '../../common/jwt';
import logger from '../../common/logger';
import { sendMail, sendSms } from '../../common/ncp';
import { generateRandomNumberKey, gqlResolver } from '../../common/utils';
import { sms as smsTemplate, email as emailTemplate } from '../../common/ncp/templates.json';
import validate from '../../common/validate';
import { findUserSocket } from '../../socket';

/** [Query] 아이디/닉네임 중복 확인 (true일 때 중복) */
const checkDuplicatedUserInfo = async (parent: any, args: CheckDuplicatedArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<CheckDuplicatedResult> => {
    const { User } = ctx.models;
    logger.debug(JSON.stringify(args));
    let user: MongooseModels.UserModel;

    switch (args.checkType) {
      case 'USERID':
        user = await User.findOne({ userId: args.value }, { _id: 1 });
        logger.debug(`[${args.checkType}] ${JSON.stringify(user)}`);
        return {
          result: !!user,
          errCode: null
        };
      case 'NICKNAME':
        user = await User.findOne({ nickname: args.value }, { _id: 1 });
        logger.debug(`[${args.checkType}] ${JSON.stringify(user)}`);
        return {
          result: !!user,
          errCode: null
        };
      default:
        return {
          result: null,
          errCode: 'SERVER_ERROR'
        };
    }
  })
);

/** [Query] 이름, 이메일 중복 확인 (계정 찾기) */
const checkDuplicatedNameAndEmail = async (parent: any, args: CheckDuplicatedNameAndEmailArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<CheckDuplicatedNameAndEmailResult> => {
    const { name, email } = args;
    const { User } = ctx.models;

    const userInfo = await User.findOne({ name, email }, { _id: 1 });

    return {
      result: !!userInfo,
      errCode: null
    };
  })
);

/** [Query] 이름, 국가코드, 휴대전화번호 중복 확인 (계정 찾기) */
const checkDuplicatedNameAndPhone = async (parent: any, args: CheckDuplicatedNameAndPhoneArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<CheckDuplicatedNameAndPhoneResult> => {
    const { name, countryCode, phone } = args;
    const { User } = ctx.models;

    const userInfo = await User.findOne({ name, countryCode: countryCode.replace('+', ''), phone }, { _id: 1 });

    return {
      result: !!userInfo,
      errCode: null
    };
  })
);

/** [Query] 로그인 */
const login = async (parent: any, args: LoginArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<LoginResult> => {
    const { models: { User, BanHistory } } = ctx;

    const filterQuery = {
      ...args,
      password: SHA256(args.password).toString(enc.Hex)
    };

    const userInfo = await User.findOne(filterQuery, { _id: 0, password: 0 });

    if (!userInfo) return {
      result: null,
      errCode: 'WRONG_LOGIN_INFO'
    };

    const banHistory = await BanHistory.findOne({ $and: [{ userUuid: userInfo.uuid }, { manualUnban: false }, { unbanDate: { $gt: new Date() } }] });

    if (!!banHistory) {
      const banDate = banHistory.banDate.toISOString();
      const unbanDate = banHistory.unbanDate.toISOString();

      return {
        result: {
          banDate,
          unbanDate
        },
        errCode: 'BANNED_USER'
      };
    }

    const loginDate = new Date();

    const accessToken = createToken({
      type: 'access',
      data: {
        uuid: Array.from(Buffer.from(userInfo.uuid.toString('hex'), 'hex')),
        loginDate: +loginDate
      }
    }, '1h');
    const refreshToken = createToken({
      type: 'refresh',
      data: {
        uuid: Array.from(Buffer.from(userInfo.uuid.toString('hex'), 'hex')),
        loginDate: +loginDate
      }
    }, '30d');

    await User.updateOne(filterQuery, { $set: { loginDate } });

    const result: LoginResult = {
      result: {
        userInfo: {
          ...userInfo.toJSON(),
          uuid: userInfo.uuid.toString('hex'),
          birth: new Date(userInfo.birth).toISOString(),
          friends: userInfo.friends.map(friend => friend.toString('hex')),
          rooms: userInfo.rooms.map(roomUuid => roomUuid.toString('hex')),
          registDate: new Date(userInfo.registDate).toISOString()
        },
        token: {
          accessToken,
          refreshToken
        }
      },
      errCode: null
    };

    logger.debug(`user::: ${JSON.stringify(userInfo)}`);
    logger.debug(`result::: ${JSON.stringify(result)}`);

    return result;
  })
);

/** [Mutation] 이메일 인증코드 발송 */
const emailVerification = async (parent: any, args: EmailVerificationArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<EmailVerificationResult> => {
    const { models: { User }, redis } = ctx;
    console.log(args);

    const { email, lang } = args;
    const isExist = !!(await User.findOne({ email }, { _id: 1 }));
    if (args.checkDuplicate && isExist) return {
      result: null,
      errCode: 'DUPLICATE_ERROR'
    };

    const uuid = uuidV4().replace(/-/gi, '');
    let authCode = generateRandomNumberKey(6);

    const title = emailTemplate.authCode.title[lang];
    const contentsTemplate = emailTemplate.authCode.contents;
    const contents = Mustache.render(contentsTemplate[lang], { authCode });
    const sendMainResult = await sendMail({ title, contents, recipients: [email] });
    await redis.authCode.setEx(uuid, sendMainResult ? authCode : '000000', 300);

    return {
      result: uuid,
      errCode: null
    };
  })
)

/** [Mutation] 이메일 인증 코드 확인 */
const checkEmailAuthCode = async (parent: any, args: CheckEmailAuthCodeArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<CheckEmailAuthCodeResult> => {
    const { models: { User }, redis } = ctx;
    const { email, authCode, uuid } = args;

    const originAuthCode = await redis.authCode.get(uuid);
    if (originAuthCode !== authCode) return {
      result: {
        success: false,
        userId: null
      },
      errCode: 'INVALID_AUTH_CODE'
    };

    await redis.authCode.del(uuid);
    await redis.verificated.setEx(uuid, email, 3600);

    const userInfo = await User.findOne({ email }, { userId: 1 });

    return {
      result: {
        success: true,
        userId: userInfo ? userInfo.userId : null
      },
      errCode: null
    };
  })
);

/** [Mutation] 휴대전화 인증코드 발송 */
const phoneVerification = async (parent: any, args: PhoneVerificationArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<PhoneVerificationResult> => {
    const { models: { User }, redis } = ctx;

    const { countryCode, phone, lang } = args;
    const isExist = !!(await User.findOne({ countryCode, phone }, { _id: 1 }));
    if (args.checkDuplicate && isExist) return {
      result: null,
      errCode: 'DUPLICATE_ERROR'
    };

    const uuid = uuidV4().replace(/-/gi, '');
    let authCode = generateRandomNumberKey(6);

    const contentsTemplate = smsTemplate.authCode[lang];
    const contents = Mustache.render(contentsTemplate, { authCode });
    const sendSmsResult = await sendSms({ countryCode, to: phone, contents });
    await redis.authCode.setEx(uuid, sendSmsResult ? authCode : '000000', 300);

    return {
      result: uuid,
      errCode: null
    };
  })
);

/** [Mutation] 휴대전화 인증코드 확인 */
const checkPhoneAuthCode = async (parent: any, args: CheckPhoneAuthCodeArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<CheckPhoneAuthCodeResult> => {
    const { models: { User }, redis } = ctx;
    const { countryCode, phone, authCode, uuid } = args;

    const originAuthCode = await redis.authCode.get(uuid);
    if (originAuthCode !== authCode) return {
      result: {
        success: false,
        userId: null
      },
      errCode: 'INVALID_AUTH_CODE'
    };

    await redis.authCode.del(uuid);
    await redis.verificated.setEx(uuid, `${countryCode}${phone}`.replace('+', ''), 3600);

    const userInfo = await User.findOne({ countryCode, phone }, { userId: 1 });

    return {
      result: {
        success: true,
        userId: userInfo ? userInfo.userId : null
      },
      errCode: null
    };
  })
);

/** [Mutation] 회원가입 */
const signUp = async (parent: any, args: SignUpArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<SignUpResult> => {
    const { redis, models: { User } } = ctx;
    const { userInfo, uuid } = args;

    const originEmail = await redis.verificated.get(uuid.email);
    if (originEmail !== userInfo.email) return {
      result: false,
      errCode: 'EMAIL_AUTH_ERROR'
    };

    const originPhone = await redis.verificated.get(uuid.phone);
    if (originPhone !== `${userInfo.countryCode}${userInfo.phone}`.replace('+', '')) return {
      result: false,
      errCode: 'PHONE_AUTH_ERROR'
    };

    if (
      !validate('userid', userInfo.userId) ||
      !validate('password', userInfo.password) ||
      !validate('nickname', userInfo.nickname) ||
      userInfo.name === ''
    ) {
      return {
        result: false,
        errCode: 'SERVER_ERROR'
      };
    }

    await User.insertMany({
      ...userInfo,
      countryCode: userInfo.countryCode.replace('+', ''),
      password: SHA256(userInfo.password).toString(enc.Hex)
    });

    await redis.verificated.del([uuid.email, uuid.phone]);

    return {
      result: true,
      errCode: null
    };
  })
);

/** [Mutation] 비밀번호 초기화 */
const resetPassword = async (parent: any, args: ResetPasswordArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<ResetPasswordResult> => {
    const { models: { User }, redis } = ctx;
    const { email, countryCode, phone, password, uuid } = args;

    const authType: 'EMAIL' | 'PHONE' | null = !!email ? 'EMAIL' : (!!countryCode && !!phone) ? 'PHONE' : null;
    if (!authType) return {
      result: false,
      errCode: 'SERVER_ERROR'
    };

    const originAuthValue = await redis.verificated.get(uuid);
    if (originAuthValue !== (authType === 'EMAIL' ? email : `${countryCode}${phone}`.replace('+', ''))) {
      return {
        result: false,
        errCode: 'WRONG_EMAIL_OR_PHONE_AUTH'
      };
    }

    const filterQuery = authType === 'EMAIL' ? { email } : { countryCode, phone };
    const userInfo = await User.findOne(filterQuery, { _id: 1 });
    if (!userInfo) return {
      result: false,
      errCode: 'SERVER_ERROR'
    };

    await User.updateOne(filterQuery, { $set: { password: SHA256(password).toString(enc.Hex) } });
    await redis.verificated.del(uuid);

    return {
      result: true,
      errCode: null
    };
  })
);

/** [Mutation] 유저 프로필 수정 */
const modifyUserProfile = async (parent: any, args: ModifyUserProfileArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<ModifyUserProfileResult> => {
    const { auth, models: { User } } = ctx;
    const { nickname } = args;

    if (auth.type !== 'access') return {
      result: null,
      errCode: 'INVALID_TOKEN'
    };

    if (!validate('nickname', nickname)) return {
      result: null,
      errCode: 'INVALID_NICKNAME'
    };

    await User.updateOne({ uuid: auth.uuid }, { $set: args });
    const userInfo = await User.findOne({ uuid: auth.uuid }, { _id: 0, password: 0, __v: 0 });
    const result: UserInfo = {
      ...userInfo.toJSON(),
      uuid: userInfo.uuid?.toString('hex'),
      birth: new Date(userInfo.birth).toISOString(),
      friends: userInfo.friends.map(friend => friend.toString('hex')),
      rooms: userInfo.rooms.map(friend => friend.toString('hex')),
      registDate: new Date(userInfo.registDate).toISOString(),
    };

    return {
      result,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 유저 정보 수정 */
const modifyUserInfo = async (parent: any, args: ModifyUserInfoArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<ModifyUserInfoResult> => {
    const { auth, models: { User }, redis } = ctx;

    const currentUserInfo = await User.findOne({ uuid: auth.uuid }, { email: 1, countryCode: 1, phone: 1 });

    const authErrorResult: ModifyUserInfoResult = {
      result: null,
      errCode: 'WRONG_EMAIL_OR_PHONE_AUTH'
    };

    const initValue = args.authType === 'EMAIL' ? currentUserInfo.email : `${currentUserInfo.countryCode}${currentUserInfo.phone}`.replace('+', '');
    const originInitValue = await redis.verificated.get(args.uuid.init);
    if (originInitValue !== initValue) return authErrorResult;

    if (args.userInfo.email !== currentUserInfo.email) {
      const email = args.userInfo.email;
      const originEmail = await redis.verificated.get(args.uuid.email);
      if (originEmail !== email) return authErrorResult;
    }

    if (
      args.userInfo.countryCode.replace('+', '') !== currentUserInfo.countryCode.replace('+', '') ||
      args.userInfo.phone !== currentUserInfo.phone
    ) {
      const phone = `${args.userInfo.countryCode}${args.userInfo.phone}`.replace('+', '');
      const originPhone = await redis.verificated.get(args.uuid.phone);
      if (phone !== originPhone) return authErrorResult;
    }

    await User.updateOne({ uuid: auth.uuid }, { $set: {
      ...args.userInfo,
      password: args.userInfo.password ? SHA256(args.userInfo.password).toString(enc.Hex) : undefined
    }});
    const newUserInfo = await User.findOne({ uuid: auth.uuid }, { _id: 0, password: 0, __v: 0 });

    await redis.verificated.del([args.uuid.init, args.uuid.email, args.uuid.phone]);

    const result: UserInfo = {
      ...newUserInfo.toJSON(),
      uuid: newUserInfo.uuid?.toString('hex'),
      countryCode: newUserInfo.countryCode.replace('+', ''),
      birth: new Date(newUserInfo.birth).toISOString(),
      friends: newUserInfo.friends.map(friend => friend.toString('hex')),
      rooms: newUserInfo.rooms.map(roomUuid => roomUuid.toString('hex')),
      registDate: new Date(newUserInfo.registDate).toISOString()
    };

    return {
      result,
      errCode: null
    };
  }, ctx)
);

/** [Query] Access Token 갱신 */
const refreshAccessToken = async (parent: any, args: RefreshAccessTokenArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<RefreshAccessTokenResult> => {
    const { auth } = ctx;
    console.log(1, auth);

    if (auth.type !== 'refresh') return {
      result: null,
      errCode: 'INVALID_TOKEN'
    };

    const newAccessToken = createToken({
      type: 'access',
      data: {
        uuid: Array.from(Buffer.from(auth.uuid.toString('hex'), 'hex')),
        loginDate: +(new Date(auth.loginDate))
      }
    }, '1h');

    return {
      result: newAccessToken,
      errCode: null
    };
  }, ctx, 'refresh')
);

/** [Mutation] 유저 삭제 (회원 탈퇴) */
const deleteUser = async (parent: any, args: any, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<DeleteUserResult> => {
    const { io, auth, models: { User, FriendRequest, Room, RoomJoinLeave, RoomName, Chat } } = ctx;

    let userInfo = await User.findOne({ uuid: auth.uuid }, { friends: 1, rooms: 1 });
    if (!userInfo) return {
      result: null,
      errCode: 'SERVER_ERROR'
    };
    const { rooms, friends } = userInfo;
    const roomUuidArr = rooms.map(d => d.toString('hex'));
    const friendUuidArr = friends.map(d => d.toString('hex'));

    await RoomName.deleteMany({ userUuid: auth.uuid });
    await RoomJoinLeave.deleteMany({ userUuid: auth.uuid });
    await Room.updateMany({ users: { $elemMatch: { $eq: auth.uuid } } }, { $pull: { users: auth.uuid } });
    await FriendRequest.deleteMany({ $or: [{ from: auth.uuid }, { to: auth.uuid }] });
    await User.updateMany({ friends: { $elemMatch: { $eq: auth.uuid } } }, { $pull: { friends: auth.uuid } });
    await User.deleteOne({ uuid: auth.uuid });

    const socket = findUserSocket(io, auth.uuid);
    socket.disconnect(true);

    _.uniq(roomUuidArr.concat(friendUuidArr)).forEach(d => {
      io.to(d).emit('refresh-friends');
      io.to(d).emit('refresh-rooms');
    });

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Query] 유저별 공지 조회 */
const getNotices = async (parent: any, args: any, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetNoticesResult> => {
    const { auth, models: { User, NoticeHistory } } = ctx;

    const now = new Date();

    const noticeHistoriesArr = await NoticeHistory.find({
      userUuid: auth.uuid,
      noticeStartDate: { $lte: now },
      noticeEndDate: { $gte: now },
      read: false
    }).sort({ registDate: -1 });

    const noticesData: NoticeData[] = noticeHistoriesArr.map(d1 => {
      return {
        uuid: d1.uuid.toString('hex'),
        noticeTitle: d1.noticeTitle,
        noticeTitleEn: d1.noticeTitleEn,
        noticeContents: d1.noticeContents,
        noticeContentsEn: d1.noticeContentsEn,
        noticeStartDate: d1.noticeStartDate.toISOString(),
        noticeEndDate: d1.noticeEndDate.toISOString()
      };
    });

    return {
      result: noticesData,
      errCode: null
    };
  }, ctx)
);

/** [Mutation] 공지 다시 보지 않기 */
const readNotice = async (parent: any, args: ReadNoticeArgs, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<ReadNoticeResult> => {
    const { auth, models: { NoticeHistory } } = ctx;
    const { noticeUuid } = args;

    await NoticeHistory.updateOne(
      { uuid: Buffer.from(noticeUuid, 'hex'), userUuid: auth.uuid },
      { $set: { read: true } }
    );

    return {
      result: true,
      errCode: null
    };
  }, ctx)
);

/** [Query] 내 정보 확인 */
const getMyInfo = async (parent: any, args: any, ctx: GraphQLContext) => (
  await gqlResolver(async (): Promise<GetMyInfoResult> => {
    const { auth, models: { User } } = ctx;

    const userInfo = await User.findOne({ uuid: auth.uuid }, { _id: 0, password: 0 });

    return {
      result: {
        ...userInfo.toJSON(),
        uuid: userInfo.uuid.toString('hex'),
        birth: new Date(userInfo.birth).toISOString(),
        friends: userInfo.friends.map(friend => friend.toString('hex')),
        rooms: userInfo.rooms.map(roomUuid => roomUuid.toString('hex')),
        registDate: new Date(userInfo.registDate).toISOString()
      },
      errCode: null
    }
  }, ctx)
);

const resolver = {
  Query: {
    checkDuplicatedUserInfo,
    checkDuplicatedNameAndEmail,
    checkDuplicatedNameAndPhone,
    emailVerification,
    phoneVerification,
    login,
    refreshAccessToken,
    getNotices,
    getMyInfo
  },
  Mutation: {
    checkEmailAuthCode,
    checkPhoneAuthCode,
    signUp,
    resetPassword,
    modifyUserProfile,
    modifyUserInfo,
    deleteUser,
    readNotice
  }
};

export default resolver;