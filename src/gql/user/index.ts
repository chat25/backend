import checkDuplicatedUserInfo from './typeDefs/query/checkDuplicatedUserInfo.gql';
import emailVerification from './typeDefs/query/emailVerification.gql';
import checkEmailAuthCode from './typeDefs/mutation/checkEmailAuthCode.gql';
import phoneVerification from './typeDefs/query/phoneVerification.gql';
import checkPhoneAuthCode from './typeDefs/mutation/checkPhoneAuthCode.gql';
import signUp from './typeDefs/mutation/signUp.gql';
import resetPassword from './typeDefs/mutation/resetPassword.gql';
import login from './typeDefs/query/login.gql';
import modifyUserProfile from './typeDefs/mutation/modifyUserProfile.gql';
import modifyUserInfo from './typeDefs/mutation/modifyUserInfo.gql';
import checkDuplicatedNameAndEmail from './typeDefs/query/checkDuplicatedNameAndEmail.gql';
import checkDuplicatedNameAndPhone from './typeDefs/query/checkDuplicatedNameAndPhone.gql';
import refreshAccessToken from './typeDefs/query/refreshAccessToken.gql';
import deleteUser from './typeDefs/mutation/deleteUser.gql';
import getNotices from './typeDefs/query/getNotices.gql';
import readNotice from './typeDefs/mutation/readNotice.gql';
import getMyInfo from './typeDefs/query/getMyInfo.gql';

import resolver from './resolver';

export const userTypeDefs = [
  checkDuplicatedUserInfo,
  emailVerification,
  checkEmailAuthCode,
  phoneVerification,
  checkPhoneAuthCode,
  signUp,
  resetPassword,
  login,
  modifyUserProfile,
  modifyUserInfo,
  checkDuplicatedNameAndEmail,
  checkDuplicatedNameAndPhone,
  refreshAccessToken,
  deleteUser,
  getNotices,
  readNotice,
  getMyInfo
];

export const userResolver = resolver;