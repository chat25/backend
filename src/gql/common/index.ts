import typeDefs from './common.gql';
import resolver from './common';

export const commonTypeDefs = typeDefs;
export const commonResolver = resolver;