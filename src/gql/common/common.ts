import { GraphQLUpload } from 'graphql-upload';

const resolver = {
  Upload: GraphQLUpload
};

export default resolver;