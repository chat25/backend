import { Server as SocketIO } from 'socket.io';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';
import _ from 'lodash';
import { getTokenPayloadData } from '../common/jwt';

/**
 * 유저 소켓 검색
 * @param io SocketIO Server
 * @param userUuid 유저 UUID
 * @returns 유저 소켓
 */
export const findUserSocket = (io: SocketIO<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, any>, userUuid: Buffer) => {
  const socketKyes = io.sockets.sockets.keys();

  for (const key of socketKyes) {
    const socket = io.sockets.sockets.get(key);
    if (socket.rooms.has(userUuid.toString('hex'))) return socket;
  }

  return null;
};

export const initializeIO = (io: SocketIO<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, any>, models: Models) => {
  io.on('connection', async socket => {
    console.log(`Connect ${socket.id}`);
    const tokenPayload: TokenPayload = getTokenPayloadData(socket.handshake.headers);
    if (!tokenPayload) return socket.disconnect();

    const { uuid } = tokenPayload
    socket.join(uuid.toString('hex'));

    try {
      const { rooms } = await models.User.findOne({ uuid }, { rooms: 1 });
      rooms.forEach(d => socket.join(d.toString('hex')));
    } catch (err) {
      console.error(err);
      return socket.disconnect();
    }

    /** 채팅방 참가 */
    socket.on('join-rooms', async () => {
      try {
        const { rooms } = await models.User.findOne({ uuid }, { rooms: 1 });
        rooms.forEach(d => socket.join(d.toString('hex')));
      } catch (err) {
        console.error(err);
      }
    });

    /** 텍스트 메시지 전송 */
    socket.on('text-msg', async (roomUuidStr: string, contents: string) => {
      const { User, Room, RoomJoinLeave, Chat } = models;
      const roomUuid = Buffer.from(roomUuidStr, 'hex');

      try {
        const roomInfo = await Room.findOne({ uuid: roomUuid });
        if (!roomInfo) return;

        if (roomInfo.type === 'personal') {
          const otherUserUuid = _.find(roomInfo.users, d => d.toString('hex') !== uuid.toString('hex'));
          const otherSocket = findUserSocket(io, otherUserUuid);
          if (otherSocket) await otherSocket.join(roomUuidStr);

          const roomUserInfoArr = await User.find({
            uuid: { $in: [uuid, otherUserUuid] },
            rooms: { $elemMatch: { $eq: roomUuid } }
          }, {
            uuid: 1
          });

          let flag1 = false;
          let flag2 = false;

          if (!_.find(roomUserInfoArr, { uuid })) {
            await User.updateOne({ uuid }, { $push: { rooms: roomUuid } });
            await RoomJoinLeave.updateOne(
              { userUuid: uuid, roomUuid },
              { $set: { joinDate: new Date(), leaveDate: null }
            });
            flag1 = true;
          }

          if (!_.find(roomUserInfoArr, { uuid: otherUserUuid })) {
            await User.updateOne({ uuid: otherUserUuid }, { $push: { rooms: roomUuid } });
            await RoomJoinLeave.updateOne(
              { userUuid: otherUserUuid, roomUuid },
              { $set: { joinDate: new Date(), leaveDate: null }
            });
            flag2 = true;
          }

          if (flag1) socket.emit('refresh-rooms');
          if (flag2) otherSocket.emit('refresh-rooms');
        }

        const [chatInfo] = await Chat.insertMany({
          userUuid: uuid,
          roomUuid,
          type: 'text',
          contents,
          read: [uuid]
        });

        await Room.updateOne({ uuid: roomUuid }, { $set: { lastChatUuid: chatInfo.uuid } });

        io.to(roomUuidStr).emit('receive-msg', {
          uuid: chatInfo.uuid.toString('hex'),
          userUuid: chatInfo.userUuid.toString('hex'),
          roomUuid: chatInfo.roomUuid.toString('hex'),
          type: chatInfo.type,
          contents: chatInfo.contents,
          filePath: chatInfo.filePath,
          sendDate: chatInfo.sendDate.toISOString(),
          read: chatInfo.read.map(d => d.toString('hex'))
        } as ChatInfo);
      } catch (err) {
        console.error(err);
      }
    });

    /** 메시지 읽기 */
    socket.on('read-msg', async (chatUuid: string) => {
      const { Chat } = models;
      const chatUuidBuffer = Buffer.from(chatUuid, 'hex');

      try {
        await Chat.updateMany(
          { uuid: chatUuidBuffer, read: { $not: { $elemMatch: { $eq: uuid } } } },
          { $push: { read: uuid } }
        );
      } catch (err) {
        console.error(err);
      }
    });

    socket.on('disconnect', () => {
      console.log('Disconnect');
    });
  });
};