namespace MongooseModels {
  interface UserModel {
    uuid: Buffer
    userId: string
    password: string
    name: string
    nickname: string
    email: string
    countryCode: string
    phone: string
    birth: Date
    gender: string
    profileImage: string
    profileText: string
    friends: Buffer[]
    rooms: Buffer[]
    registDate: Date
    loginDate: Date
  }

  interface FriendRequestModel {
    uuid: Buffer
    from: Buffer
    to: Buffer
    requestDate: Date
  }

  interface PersonalRoom {
    uuid: Buffer
    userUuid?: null
    type: 'personal'
    name?: null
    users?: Buffer[]
    maxUser?: null
    isPrivate?: null
    password?: null
    lastChatUuid: Buffer
    blackList?: null
    createDate?: Date
  }
  
  interface GroupRoom {
    uuid: Buffer
    userUuid?: null
    type: 'group'
    name?: null
    users: Buffer[]
    maxUser?: null
    isPrivate?: null
    password: null
    lastChatUuid: Buffer
    blackList: Buffer[]
    createDate?: Date
  }
  
  interface OpenRoom {
    uuid: Buffer
    userUuid: Buffer
    type: 'open'
    name: string
    users: Buffer[]
    maxUser: number
    isPrivate: boolean
    password: string
    lastChatUuid: Buffer
    blackList: Buffer[]
    createDate?: Date
  }

  type RoomModel = PersonalRoom | GroupRoom | OpenRoom;

  interface JoinLeaveMessage {
    uuid: Buffer
    userUuid: Buffer
    roomUuid: Buffer
    type: 'join' | 'leave'
    contents: null
    filePath: string
    sendDate: Date
    read: Buffer[] | null
  }
  
  interface ChatMessage {
    uuid: Buffer
    userUuid: Buffer
    roomUuid: Buffer
    type: 'text' | 'file' | 'image' | 'video' | 'audio'
    contents: string
    filePath: string
    sendDate: Date
    read: Buffer[]
  }

  type ChatModel = JoinLeaveMessage | ChatMessage;

  interface RoomNameModel {
    uuid: Buffer
    userUuid: Buffer
    roomUuid: Buffer
    roomName: string
  }

  interface RoomJoinLeaveModel {
    uuid: Buffer
    userUuid: Buffer
    roomUuid: Buffer
    joinDate: Date
    leaveDate: Date
  }

  interface BanHistoryModel {
    uuid: Buffer
    userUuid: Buffer
    banDate: Date
    unbanDate: Date
    manualUnban: boolean
    updateDate: Date
  }

  interface NoticeHistoryModel {
    uuid: Buffer
    userUuid: Buffer
    noticeTitle: string
    noticeTitleEn: string
    noticeContents: string
    noticeContentsEn: string
    noticeStartDate: Date
    noticeEndDate: Date
    read: boolean
    registDate: Date
  }
}