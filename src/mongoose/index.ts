import mongoose, { Mongoose } from 'mongoose';
import _ from 'lodash';
import UserSchema from './schema/User';
import FriendRequestSchema from './schema/FriendRequest';
import RoomSchema from './schema/Room';
import RoomJoinLeaveSchema from './schema/RoomJoinLeave';
import RoomNameSchema from './schema/RoomName';
import ChatSchema from './schema/Chat';
import BanHistorySchema from './schema/BanHistory';
import NoticeHistorySchema from './schema/NoticeHistory';
import { commonConfig } from '../common/config';

/** 모델명 정의 */
export const modelNames = {
  User: 'User',
  FriendRequest: 'FriendRequest',
  Room: 'Room',
  RoomJoinLeave: 'RoomJoinLeave',
  RoomName: 'RoomName',
  Chat: 'Chat',
  BanHistory: 'BanHistory',
  NoticeHistory: 'NoticeHistory'
};

mongoose.model(modelNames.User, UserSchema);
mongoose.model(modelNames.FriendRequest, FriendRequestSchema);
mongoose.model(modelNames.Room, RoomSchema);
mongoose.model(modelNames.RoomJoinLeave, RoomJoinLeaveSchema);
mongoose.model(modelNames.RoomName, RoomNameSchema);
mongoose.model(modelNames.Chat, ChatSchema);
mongoose.model(modelNames.BanHistory, BanHistorySchema);
mongoose.model(modelNames.NoticeHistory, NoticeHistorySchema);

/**
 * Mongoose 커넥션 및 모델 생성
 * @returns Mongoose Connection & Models
 */
export const createMongoConnection = async () => {
  const {
    mongodb: {
      host,
      port,
      options
    }
  } = commonConfig;

  const mongo: Mongoose = await mongoose.connect(`mongodb://${host}:${port}`, options);
  const tmp: any = {};
  Object.keys(modelNames).forEach(key => tmp[modelNames[key]] = mongo.connection.model(modelNames[key]));
  const models: Models = _.clone(tmp);

  return {
    mongo,
    models
  };
};