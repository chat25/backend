import { Schema } from 'mongoose';
import { generateUuidBuffer } from '../../common/utils';

const FriendRequestSchema = new Schema({
  uuid: { type: Buffer, index: true, unique: true, default: () => generateUuidBuffer() },
  from: { type: Buffer, indexe: true },
  to: { type: Buffer, index: true },
  requestDate: { type: Date, default: () => new Date() }
})
.index({ from: 1, to: 1 }, { unique: true });

export default FriendRequestSchema;