import { Schema } from 'mongoose';
import { generateUuidBuffer } from '../../common/utils';

const NoticeHistorySchema = new Schema({
  uuid: { type: Buffer, index: true, unique: true, default: () => generateUuidBuffer() },
  userUuid: { type: Buffer, index: true, default: null },
  noticeTitle: { type: String, default: '' },
  noticeTitleEn: { type: String, default: '' },
  noticeContents: { type: String, default: '' },
  noticeContentsEn: { type: String, default: '' },
  noticeStartDate: { type: Date },
  noticeEndDate: { type: Date },
  read: { type: Boolean, default: false },
  registDate: { type: Date, default: new Date() }
})
.index({ registDate: -1 })
.index({ userUuid: 1, noticeStartDate: 1, noticeEndDate: 1, read: 1 })
.index({ uuid: 1, userUuid: 1 });

export default NoticeHistorySchema;