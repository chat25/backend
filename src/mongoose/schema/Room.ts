import { Schema } from 'mongoose';
import { generateUuidBuffer } from '../../common/utils';

const TYPE_PERSONAL = 'personal';
const TYPE_GROUP = 'group';
const TYPE_OPEN = 'open';

const RoomSchema = new Schema({
  uuid: { type: Buffer, index: true, unique: true, default: () => generateUuidBuffer() },
  userUuid: { type: Buffer, index: true, default: null },
  type: { type: String, index: true, enum: [TYPE_PERSONAL, TYPE_GROUP, TYPE_OPEN] },
  name: { type: String, index: true, default: null },
  users: { type: [Buffer], index: true },
  maxUser: { type: Number, default: null },
  isPrivate: { type: Boolean, default: false },
  password: { type: String, default: null },
  lastChatUuid: { type: Buffer, default: null },
  blackList: { type: [Buffer], default: null },
  createDate: { type: Date, default: () => new Date() }
})
.index({ type: 1, users: 1 })
.index({ uuid: 1, lastChatUuid: 1 })
.index({ uuid: 1, type: 1, name: 1, isPrivate: 1, blackList: 1 });

export default RoomSchema;