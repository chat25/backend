import { Schema } from 'mongoose';
import { generateUuidBuffer } from '../../common/utils';

const RoomJoinLeaveSchema = new Schema({
  uuid: { type: Buffer, index: true, unique: true, default: () => generateUuidBuffer() },
  userUuid: { type: Buffer, index: true },
  roomUuid: { type: Buffer, index: true },
  joinDate: { type: Date, default: null },
  leaveDate: { type: Date, default: null }
})
.index({ userUuid: 1, roomUuid: 1 }, { unique: true });

export default RoomJoinLeaveSchema;