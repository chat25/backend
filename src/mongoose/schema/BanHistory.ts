import { Schema } from 'mongoose';
import { generateUuidBuffer } from '../../common/utils';

const BanHistorySchema = new Schema({
  uuid: { type: Buffer, index: true, unique: true, default: () => generateUuidBuffer() },
  userUuid: { type: Buffer, index: true, default: null },
  banDate: { type: Date, default: new Date() },
  unbanDate: { type: Date },
  manualUnban: { type: Boolean, default: false },
  updateDate: { type: Date, default: new Date() }
})
.index({ updateDate: -1 })
.index({ manualUnban: 1, unbanDate: 1 })
.index({ userUuid: 1, manualUnban: 1, unbanDate: 1 })
.index({ userUuid: 1, banDate: 1, unbanDate: 1, updateDate: 1 })
;

export default BanHistorySchema;