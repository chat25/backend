import { Schema } from 'mongoose';
import { generateUuidBuffer } from '../../common/utils';

const TYPE_JOIN = 'join';
const TYPE_LEAVE = 'leave';
const TYPE_TEXT = 'text';
const TYPE_FILE = 'file';
const TYPE_IMAGE = 'image';
const TYPE_VIDEO = 'video';
const TYPE_AUDIO = 'audio';

const ChatSchema = new Schema({
  uuid: { type: Buffer, index: true, unique: true, default: () => generateUuidBuffer() },
  userUuid: { type: Buffer, index: true },
  roomUuid: { type: Buffer, index: true },
  type: { type: String, enum: [TYPE_JOIN, TYPE_LEAVE, TYPE_TEXT, TYPE_FILE, TYPE_IMAGE, TYPE_VIDEO, TYPE_AUDIO] },
  contents: { type: String, default: null },
  filePath: { type: String, default: '' },
  sendDate: { type: Date, default: () => new Date() },
  read: { type: [Buffer], default: [] }
})
.index({ sendDate: -1 })
.index({ uuid: 1, read: 1 })
.index({ roomUuid: 1, sendDate: -1 })
.index({ roomUuid: 1, read: 1 });

export default ChatSchema;