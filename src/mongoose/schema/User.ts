import { Schema } from 'mongoose';
import { generateUuidBuffer } from '../../common/utils';

const UserSchema = new Schema({
  uuid: { type: Buffer, index: true, unique: true, default: () => generateUuidBuffer() },
  userId: { type: String, index: true, unique: true },
  password: { type: String },
  name: { type: String },
  nickname: { type: String, index: true, unique: true },
  email: { type: String, index: true, unique: true },
  countryCode: { type: String },
  phone: { type: String },
  birth: { type: Date },
  gender: { type: String },
  profileImage: { type: String, default: null },
  profileText: { type: String, default: '' },
  friends: { type: [Buffer], index: true, default: [] },
  rooms: { type: [Buffer], index: true, default: [] },
  registDate: { type: Date, default: () => new Date() },
  loginDate: { type: Date, default: () => null }
})
.index({ userId: 1, password: 1 })
.index({ countryCode: 1, phone: 1 })
.index({ name: 1, email: 1 }, { unique: true })
.index({ name: 1, countryCode: 1, phone: 1 }, { unique: true })
.index({ uuid: 1, loginDate: 1 })
.index({ uuid: 1, nickname: 1 })
.index({ uuid: 1, friends: 1 })
.index({ uuid: 1, rooms: 1 })
.index({ uuid: 1, loginDate: 1 });

export default UserSchema;