import { Schema } from 'mongoose';
import { generateUuidBuffer } from '../../common/utils';

const RoomNameSchema = new Schema({
  uuid: { type: Buffer, index: true, unique: true, default: () => generateUuidBuffer() },
  userUuid: { type: Buffer, index: true, default: null },
  roomUuid: { type: Buffer, index: true, default: null },
  roomName: { type: String }
})
.index({ userUuid: 1, roomUuid: 1 }, { unique: true });

export default RoomNameSchema;