import { Router, Request } from 'express';
import iconv from 'iconv-lite';
import { path as appRoot } from 'app-root-path';
import fs from 'fs';
import path from 'path';
import mime from 'mime';

const router = Router();

/** 브라우저별 다운로드 파일 이름 생성 */
const getDownloadFileName = (req: Request, fileName: string) => {
  const userAgent = req.headers['user-agent'];

  if (
    userAgent.indexOf('MSIE') > -1
    || userAgent.indexOf('Trident') > -1
  ) return encodeURIComponent(fileName).replace(/\\+/gi, '%20');

  if (
    userAgent.indexOf('Chrome') > -1
    || userAgent.indexOf('Opera') > -1
    || userAgent.indexOf('Firefox') > -1
    || userAgent.indexOf('Safari') > -1
  ) return iconv.decode(iconv.encode(fileName, 'UTF-8'), 'ISO-8859-1');

  return fileName;
};

/** 다운로드 Router */
const downloadRouter = (models: Models) => {
  router.get('/chat/:chatUuid', async (req, res) => {
    const { Chat } = models;
    const { chatUuid } = req.params;
    const chatUuidBuffer = Buffer.from(chatUuid, 'hex');

    try {
      const chatInfo = await Chat.findOne({ uuid: chatUuidBuffer }, { filePath: 1 });
      console.log(chatInfo);
      if (!chatInfo || !chatInfo.filePath || !fs.existsSync(path.join(appRoot, chatInfo.filePath))) throw 'SERVER_ERROR';

      const filePath = path.join(appRoot, chatInfo.filePath);
      let fileName = path.basename(filePath);
      fileName = fileName.substring(fileName.indexOf('_') + 1);
      const mimeType = mime.lookup(fileName);

      res.setHeader('Content-Disposition', `attachment; filename=${getDownloadFileName(req, fileName)}`);
      res.setHeader('Content-Type', mimeType);
      res.setHeader('Content-Length', fs.lstatSync(filePath).size);

      const readStream = fs.createReadStream(filePath);
      readStream.pipe(res);
    } catch (err) {
      console.error(err);
      res.status(500).send(err + '');
    }
  });

  return router;
};

export default downloadRouter;