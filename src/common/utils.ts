import _ from 'lodash';
import { v4 as uuidV4 } from 'uuid';
import os from 'os';
import { commonConfig } from './config';
import logger from './logger';

/**
 * @returns 현재 서버 Ipv4 주소 리스트
 */
export const getIpv4Addresses = () => {
  const networkInterfaces = os.networkInterfaces();
  const keys = Object.keys(networkInterfaces);
  const result = [];

  for (const key of keys) {
    const networkInterface = _.find(networkInterfaces[key], { family: 'IPv4' });
    if (!networkInterface) continue;
    result.push(networkInterface.address);
  }

  return result;
};

/** Http Server 실행 콜백 함수 */
export const initHttpServerCallback = () => {
  console.log(`\n[ ${process.env.NODE_ENV} ]`);
  console.log('=============================');
  getIpv4Addresses().forEach((ip: string) => console.log(` http://${ip}:${commonConfig.httpServer.port}`));
  console.log('=============================\n');
};

/**
 * @param uuid 기존 UUID (없을 시 새로운 UUID 생성)
 * @returns 바이너리 UUID
 */
export const generateUuidBuffer = (uuid?: string) => {
  const hexUuid = (uuid || uuidV4()).replace(/-/gi, '');
  const uuidBuffer = Buffer.from(hexUuid, 'hex');
  return uuidBuffer;
};

/**
 * GraphQL Resolver 랩핑 함수
 * @param cb 콜백 함수
 * @param ctx GraphQL Context (Access Token 확인 시에만 입력)
 * @param tokenType 토큰 타입
 * @returns 콜백 함수 리턴값
 */
export const gqlResolver = async (cb: () => Promise<any>, ctx?: GraphQLContext, tokenType: 'access' | 'refresh' = 'access') => {
  if (!!ctx) {
    const { isAdmin, auth, models: { User } } = ctx;
    console.log('gqlResolver', isAdmin, auth);

    const errResult = {
      result: null,
      errCode: 'INVALID_TOKEN'
    };

    if (!isAdmin && !auth) return errResult;

    if (!isAdmin) {
      if (!auth || !auth.uuid || auth.type !== tokenType) return errResult;
  
      const isValidUser = await User.countDocuments({ uuid: auth.uuid, loginDate: new Date(auth.loginDate) });
      if (!isValidUser) return errResult;
    }
  }

  try {
    const result = await cb();
    return result;
  } catch (err) {
    console.error(err);
    logger.error(err + '');
    return {
      result: null,
      errCode: 'SERVER_ERROR'
    };
  }
};

/** 
 * @param len 랜덤 숫자키 길이
 * @returns 랜덤 숫자키
 */
export const generateRandomNumberKey = (len: number) => {
  let numberKeyArr = [];

  if (process.env.NODE_ENV === 'test') {
    for (let i = 0; i < len; i++) {
      numberKeyArr.push(0);
    }
  } else {
    while (numberKeyArr.length < len) {
      const num = Math.floor(Math.random() * 10);
      numberKeyArr.push(num);
    }
  }

  return numberKeyArr.join('');
};