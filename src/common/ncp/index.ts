import CryptoJS from 'crypto-js';
import axios from 'axios';
import { ncpConfig } from '../config';

/**
 * Signature 생성
 * @param timestamp
 * * 1970년 1월 1일 00:00:00 협정 세계시(UTC)부터의 경과 시간을 밀리초(Millisecond)로 나타낸 것
 * * API Gateway 서버와 시간 차가 5분 이상 나는 경우 유효하지 않은 요청으로 간주
 * @returns Signature String
 */
const createSignature = (service: 'sms' | 'mail', timestamp: string) => {
  if (!ncpConfig) return null;

  const {
    accessKey,
    secretKey,
    serviceId
  } = ncpConfig;

  const space = ' ';
  const newLine = '\n';
  const method = 'POST';
  const url = (() => {
    switch (service) {
      case 'sms':
        return `/sms/v2/services/${serviceId}/messages`;
      case 'mail':
        return '/api/v1/mails';
      default:
        return null;
    }
  })();
  if (!url) return;

  const hmac = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA256, secretKey);
  hmac.update(method);
  hmac.update(space);
  hmac.update(url);
  hmac.update(newLine);
  hmac.update(timestamp);
  hmac.update(newLine);
  hmac.update(accessKey);

  const hash = hmac.finalize();

  return hash.toString(CryptoJS.enc.Base64);
};

/**
 * SMS 전송
 * @param param
 * * countryCode: 국가전화코드 (default '+82')
 * * to: SMS를 보낼 번호
 * * contents: SMS 내용
 * @returns SMS 전송 결과
 */
export const sendSms = async ({
  countryCode,
  to,
  contents
}: SendSMSParams) => {
  if (!ncpConfig) return null;

  const {
    accessKey,
    serviceId,
    phoneNum
  } = ncpConfig;

  const timeStamp = Date.now() + '';

  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    'x-ncp-apigw-timestamp': timeStamp,
    'x-ncp-iam-access-key': accessKey,
    'x-ncp-apigw-signature-v2': createSignature('sms', timeStamp)
  };

  const body = {
    type: 'SMS',
    contentType: 'COMM',
    countryCode: countryCode || '+82',
    from: phoneNum,
    content: contents,
    messages: [{ to }]
  };

  const { data } = await axios({
    url: `https://sens.apigw.ntruss.com/sms/v2/services/${serviceId}/messages`,
    method: 'POST',
    headers,
    data: body
  });

  return data;
};

/** 메일 전송 */
export const sendMail = async ({
  title,
  contents,
  recipients
}: SendMailParams) => {
  if (!ncpConfig) return null;

  const {
    accessKey,
    email,
    emailSenderName
  } = ncpConfig;

  const timeStamp = Date.now() + '';

  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    'x-ncp-apigw-timestamp': timeStamp,
    'x-ncp-iam-access-key': accessKey,
    'x-ncp-apigw-signature-v2': createSignature('mail', timeStamp)
  };

  const body = {
    senderAddress: email,
    senderName: emailSenderName,
    title,
    body: contents,
    recipients: recipients.map(address => ({ address, type: 'R' }))
  };

  const { data } = await axios({
    url: `https://mail.apigw.ntruss.com/api/v1/mails`,
    method: 'POST',
    headers,
    data: body
  });

  return data;
};