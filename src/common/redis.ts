import { createClient, RedisClientType, RedisClientOptions } from 'redis';
import logger from './logger';
import { commonConfig } from './config';

class Redis {
  dbNum: number;
  client: RedisClientType;

  constructor(dbNum: number) {
    this.dbNum = dbNum;
  }

  async connect() {
    const clientOptions: RedisClientOptions = {
      ...commonConfig.redis.options,
      database: this.dbNum
    };

    try {
      this.client = createClient(clientOptions);
      await this.client.connect();

      logger.info(`Redis Connect Success: DB ${clientOptions.database}`);
    } catch (err) {
      console.error(err);
      logger.error(`Redis Connect Fail ...: DB ${clientOptions.database}`);
      process.exit(-1);
    }
  }

  async get(key: string) {
    try {
      const result = await this.client.get(key);
      return result;
    } catch (err) {
      logger.error(err);
      throw err;
    }
  }

  async setEx(key: string, value: string, exp: number) {
    try {
      await this.client.setEx(key, exp, value);
    } catch (err) {
      logger.error(err);
      throw err;
    }
  }

  async del(key: string | string[]) {
    try {
      await this.client.del(key);
    } catch (err) {
      logger.error(err);
      throw err;
    }
  }
}

export default Redis;