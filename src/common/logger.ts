import { path as appRoot } from 'app-root-path';
import winston from 'winston';
import expressWinston from 'express-winston';
import 'winston-daily-rotate-file';
import moment from 'moment';
import path from 'path';

const { combine, timestamp, label, printf } = winston.format;

const logFormat = printf(({ level, message, label, timestamp }) => {
  return `[${moment(timestamp).format('YYYY-MM-DD HH:mm:ss')}] [${label}] [${level.toUpperCase()}] ${message}`;
});

const options = {
  development: {
    level: 'debug',
    handleException: true,
    json: false,
    colorize: false,
    format: combine(
      label({ label: 'SERVER' }),
      timestamp(),
      logFormat
    )
  },
  production: {
    level: 'info',
    filename: path.join(appRoot, 'logs', 'server_%DATE%.log'),
    handleException: true,
    json: false,
    colorize: false,
    datePattern: 'YYYYMMDD',
    zippedArchive: true,
    maxFiles: '30d',
    format: combine(
      label({ label: 'SERVER' }),
      timestamp(),
      logFormat
    )
  }
};

const transports = [
  new winston.transports.Console(options.development),
  process.env.NODE_ENV === 'production' && new winston.transports.DailyRotateFile(options.production)
].filter(d => !!d);

/** Winston Logger */
const logger = winston.createLogger({
  transports,
  exitOnError: false
});

/** Express Logger Middleware */
export const loggerMiddleware = expressWinston.logger({
  transports,
  msg: `HTTP {{req.socket.remoteAddress}} "{{req.method}} {{req.url}}" {{res.statusCode}} - "{{req.headers['user-agent']}}"`
});

export default logger;