import YAML from 'yaml';
import { path as appRoot } from 'app-root-path';
import path from 'path';
import fs from 'fs';
import logger from './logger';

const configPath = path.join(appRoot, 'config');

const readYamlFile = (filePath: string) => {
  if (!fs.existsSync(filePath)) return null;
  return YAML.parse(fs.readFileSync(filePath).toString('utf8'));
};

/** 공통 Config 파일 로드 (common.yml) */
export const commonConfig: CommonConfig = readYamlFile(path.join(configPath, 'common.yml'))[process.env.NODE_ENV];

if (!commonConfig) {
  logger.error('No Common Config File (/config/common.yml)');
  process.exit(-1);
}

/** Naver Cloud Platform Config 파일 로드 (ncp.yml) */
export const ncpConfig: NCPConfig = readYamlFile(path.join(configPath, 'ncp.yml'));