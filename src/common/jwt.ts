import jwt from 'jsonwebtoken';
import { path as appRoot } from 'app-root-path';
import { AES, enc } from 'crypto-js';
import fs from 'fs';
import path from 'path';
import logger from './logger';
import { commonConfig } from './config';
import { IncomingHttpHeaders } from 'http';

const publicKey = fs.readFileSync(path.join(appRoot, 'rsa', 'public.pem'));
const privateKey = fs.readFileSync(path.join(appRoot, 'rsa', 'private.pem'));
const aesKey = Buffer.from(commonConfig.key, 'hex').toString('utf8');

/**
 * RSA256 암호화 JWT 생성
 * @param data Payload Object
 * @param exp 만료기간 (ex. '30m', '1h', '1d', 3600, ...)
 * @returns JWT
 */
export const createToken = (data: object, exp?: string | number) => {
  console.log({data,exp});
  const token = jwt.sign(
    { data: AES.encrypt(JSON.stringify(data), aesKey).toString() },
    privateKey,
    { algorithm: 'RS256', expiresIn: exp }
  );
  return token;
};

/**
 * 토큰 유효성 검사, JWT Payload 리턴
 * @param token JWT
 * @returns JWT Payload
 */
export const verifyToken = (token: string) => {
  try {
    const payload: any = jwt.verify(token, publicKey, { algorithms: ['RS256'] });
    payload.data = JSON.parse(AES.decrypt(payload.data, aesKey).toString(enc.Utf8));
    return payload;
  } catch (err) {
    logger.error(err);
    return null;
  }
};

/**
 * Request 헤더의 authorization 토큰 확인
 * @param headers Express Request Headers
 * @returns Decoded JWT Payload
 */
export const getTokenPayloadData = (headers: IncomingHttpHeaders): TokenPayload | undefined => {
  const { authorization } = headers;
  if (!authorization || authorization.indexOf('Bearer') !== 0) return;

  const token = authorization.replace('Bearer', '').trim();
  const payload = verifyToken(token);
  if (!payload) return;

  const payloadData: TokenPayloadData = payload.data;
  if (['access', 'refresh'].indexOf(payloadData.type) < 0) return;

  const result: TokenPayload = {
    type: payloadData.type,
    uuid: Buffer.from(payloadData.data.uuid),
    loginDate: new Date(payloadData.data.loginDate).toISOString()
  };

  return result;
};

/**
 * Request 헤더의 authorization 토큰 확인 (관리자 토큰)
 * @param headers Express Request Headers
 * @returns isAdmin
 */
export const validateAdminToken = (headers: IncomingHttpHeaders): boolean => {
  // if (process.env.NODE_ENV === 'development') return true;

  const { authorization } = headers;
  if (!authorization || authorization.indexOf('Bearer') !== 0) return;

  const token = authorization.replace('Bearer', '').trim();
  const payload = verifyToken(token);
  if (!payload) return;

  const payloadData = payload.data;
  if (payloadData.data.admin !== commonConfig.admin) return;

  return true;
};