import { Request, Response } from 'express';
import {
  Mongoose,
  ConnectOptions,
  Model
} from 'mongoose';
import { RedisClientOptions } from 'redis';
import { Server as SocketIO } from 'socket.io';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';
import { FileUpload } from 'graphql-upload';
import Redis from './common/redis';

declare global {
  interface CommonConfig {
    key: string
    admin: string
    httpServer: {
      host: string
      port: number
      backlog: number
      cors: boolean
    }
    graphql: {
      path: string
      playground: boolean
      debug: boolean
      introspection: boolean
    }
    mongodb: {
      host: string
      port: number
      options: ConnectOptions
    }
    redis: {
      options: RedisClientOptions
      database: {
        authCode: number
        verificated: number
      }
    }
  }

  interface NCPConfig {
    accessKey: string
    secretKey: string
    serviceId: string
    phoneNum: string
    email: string
    emailSenderName: string
  }

  interface Models {
    User: Model<MongooseModels.UserModel>
    FriendRequest: Model<MongooseModels.FriendRequestModel>
    Room: Model<MongooseModels.RoomModel>
    RoomJoinLeave: Model<MongooseModels.RoomJoinLeaveModel>
    RoomName: Model<MongooseModels.RoomNameModel>
    Chat: Model<MongooseModels.ChatModel>
    BanHistory: Model<MongooseModels.BanHistoryModel>
    NoticeHistory: Model<MongooseModels.NoticeHistoryModel>
  }

  interface TokenPayloadData {
    type: 'access' | 'refresh'
    data: {
      uuid: number[]
      loginDate: number
    }
  }

  interface TokenPayload {
    type: 'access' | 'refresh'
    uuid: Buffer
    loginDate: string
  }

  interface GraphQLContext {
    req: Request
    res: Response
    mongo: Mongoose
    models: Models
    redis: RedisDB
    io: SocketIO<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, any>
    auth?: TokenPayload
    isAdmin?: boolean
  }

  interface SendSMSParams {
    countryCode?: string
    to: string
    contents: string
  }

  interface SendMailParams {
    title: string
    contents: string
    recipients: string[]
  }

  interface RedisDB {
    authCode: Redis
    verificated: Redis
  }

  type GraphQLFileUpload = FileUpload;

  type UploadType = 'image' | 'video' | 'audio' | 'file';
}

export {};