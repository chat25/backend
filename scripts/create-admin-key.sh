#!/bin/bash
echo -n 'Admin ID : ' && read adminId
echo -n 'Admin Password : ' && read -s password
echo
echo '============================================================================'
adminKey=$(echo -n "$adminId$password" | openssl dgst -sha256 | sed 's/(stdin)= //g')
echo "Admin Key : $adminKey"

export ADMIN_KEY=$adminKey