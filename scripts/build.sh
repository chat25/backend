#!/bin/bash
mkdir build
mv dist build
cp -r \
  package.json \
  package-lock.json \
  scripts/startup.sh \
  config \
  rsa \
build
tar cvzf build.tgz build