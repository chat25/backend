#!/bin/bash
export NODE_ENV=production

if [ ! -d node_modules ]; then
  npm i
fi

node dist