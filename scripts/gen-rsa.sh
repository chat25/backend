#!/bin/sh
rm -rf rsa
mkdir rsa
openssl genrsa -out rsa/private.pem 2048
openssl rsa -in rsa/private.pem -pubout > rsa/public.pem