# Chat Backend
## 1. 개발 환경
- OS : Ubuntu 18.04 이상(WSL2 포함), MacOS
- NodeJS : 14 버전
- openssl 설치 필요

## 2. 설치 및 실행
```bash
> git clone https://gitlab.com/chat25/backend.git backend
> cd backend
```

RSA Public/Private 키 생성
```bash
> npm run gen-rsa
```

관리자 키 생성 (Default: admin/admin)
```bash
> npm run create-admin-key

Admin ID : admin      # 사용할 관리자 아이디 입력
Admin Password :      # 사용할 관리자 비밀번호 입력
============================================================================
Admin Key : d82494f05d6917ba02f7aaa29689ccb444bb73f20380876cb05d1f37537b7892
```

Backend 개발 설정 변경  
( MongoDB, Redis 설정 변경 필요 )
```bash
> cp config/common-default.yml config/common.yml
> vi config/common.yml

## 개발 설정
development:
  key: '703273357638792F413F4428472B4B6250655368566D597133743677397A2443'
  admin: 'd82494f05d6917ba02f7aaa29689ccb444bb73f20380876cb05d1f37537b7892' # 생성한 관리자 키
  httpServer:
    host: '0.0.0.0'
    port: 8080
    backlog: 100
    cors: true
  graphql:
    path: /gql
    playground: true
    debug: true
    introspection: true
  mongodb:
    host: '' # MongoDB Host
    port: 27017
    options:
      dbName: chat_dev
      auth:
        username: root
        password: '' # MongoDB 비밀번호
      authSource: admin
      readPreference: primary
      minPoolSize: 10
      maxPoolSize: 100
  redis:
    options:
      socket:
        host: '' # Redis Host
        port: 6379
      password: '' # Redis 비밀번호
    database:
      authCode: 0
      verificated: 3
...
```

Naver Cloud Platform API 설정  
( 휴대전화, 이메일 인증 기능을 사용하지 않을 시 생략 가능하며 회원가입, 계정 찾기, 유저 정보 변경 시 휴대전화, 이메일 인증 코드는 '<b>000000</b>'을 입력하시면 됩니다. )
```bash
> cp config/ncp-default.yml config/ncp.yml
> vi config/ncp.yml

## 네이버 클라우드 플랫폼 API
## - Simple & Easy Notification Service 이용 신청 필요
## - Cloud Outbound Mailer 이용 신청 필요

# 마이페이지 > 인증키 관리 > Access Key ID
accessKey: ''

# 마이페이지 > 인증키 관리 > Secret Key
secretKey: ''

# 서비스 > Application Services > Simple & Easy Notification Service > SMS 메뉴 > Calling Number 서브 메뉴
# > 프로젝트 생성 및 발신번호 등록 > Project 서브메뉴 > 서비스 ID
serviceId: ''

# Simple & Easy Notification Service에서 등록한 발신 번호
phoneNum: ''

# 규격에 맞는 임의의 이메일 주소 (이메일 발신 용도)
email: ''

# 임의의 발신자 이름 (이메일 발신 용도)
emailSenderName: ''
```

개발 서버 실행
```bash
> npm i
> npm run dev
```