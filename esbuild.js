const { build } = require('esbuild');
const { nodeExternalsPlugin } = require('esbuild-node-externals');
const { default: graphqlLoaderPlugin } = require('@luckycatfactory/esbuild-graphql-loader');

build({
  entryPoints: ['src/index.ts'],
  platform: 'node',
  bundle: true,
  minify: true,
  outfile: 'dist/index.js',
  plugins: [
    nodeExternalsPlugin(),
    graphqlLoaderPlugin({ filterRegex: /\.gql$/ })
  ]
})
.then(r => console.log(r))
.catch(err => {
  console.error(err);
  process.exit(-1);
});